package com.example.backend.controller.services;

import com.example.backend.model.User;
import com.example.backend.repository.UserRepository;
import com.example.backend.securityconfig.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AdminService {

    @Autowired
    private UserRepository adminRepository;

    public Optional<User> getAdminProfile(String username) {
        return adminRepository.findByUsername(username);
    }
}
