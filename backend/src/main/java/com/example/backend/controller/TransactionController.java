package com.example.backend.controller;

import com.example.backend.controller.services.TransactionService;
import com.example.backend.dto.TransactionDto;
import com.example.backend.helper.PDFExporter;
import com.example.backend.message.ResponseMessage;
import com.example.backend.model.Product;
import com.example.backend.model.Transaction;
import com.lowagie.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/transaction")
@CrossOrigin(origins = "http://localhost:3000")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

//    @PostMapping("{user_id}/{card_id}/{subtotal}")
//    public ResponseEntity<ResponseMessage> saveTransaction(@PathVariable("user_id") Integer user_id,
//                                                           @PathVariable("card_id") Integer card_id,
//                                                           @PathVariable("subtotal") Double subtotal,
//                                                           @RequestBody List<Integer> cartList) {
//        Transaction transaction = new Transaction(user_id, subtotal, card_id);
//        System.out.println(transaction);
//        System.out.println(cartList);
////        if (transactionService.save(transaction))
//            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("Transaction complete!"));
////        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Transaction fail!"));
//    }

    @PostMapping("")
    public ResponseEntity<ResponseMessage> saveTransaction(@RequestBody Transaction transaction) {
        System.out.println(transaction);
        Integer trans_id = transactionService.save(transaction);
        if (trans_id > 0)
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(""+trans_id));
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Transaction fail!"));
    }

    @GetMapping("")
    public ResponseEntity<Page<Transaction>> transactionList(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        try {
            return new ResponseEntity<>(transactionService.getAllTransaction(page, size), HttpStatus.OK);
        }catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

//    @GetMapping("/date")
//    public ResponseEntity<Page<TransactionDto>> transactionListByDate(@RequestParam("min") Date min, @RequestParam("max") Date max, @RequestParam("page") Integer page, @RequestParam("size") Integer size) {
//        try {
//            return new ResponseEntity<>(transactionService.getAllByDate(min, max, page, size), HttpStatus.OK);
//        }catch (NullPointerException e) {
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//    }

    @GetMapping("/date/pdf")
    public void exportToPDF(@RequestParam("start") Date start, @RequestParam("end") Date end, HttpServletResponse response) throws DocumentException, IOException {
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        System.out.println(start + " " + end);
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=transaction_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        List<TransactionDto> listUsers = transactionService.getAllByDateExport(start, end);

        PDFExporter exporter = new PDFExporter(listUsers);
        exporter.export(response, start, end);

    }
}
