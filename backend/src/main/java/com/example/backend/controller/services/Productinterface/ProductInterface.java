package com.example.backend.controller.services.Productinterface;

import com.example.backend.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Slice;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface ProductInterface {
     Boolean save(MultipartFile file, int admin_id, Date createdAt);
     Page<Product> getAllProduct(Integer page, Integer size);
     Page<Product> getAllProductSorted(Integer page, Integer size, String sort);
     Product get(Integer id);
     Slice<Product> searchProduct(String search, Integer page, Integer size);
     Slice<Product> searchProductSorted(String search, Integer page, Integer size, String sort);
     void deleteById(Integer productId);
     Slice<Product> getProductNameSorted(String search, Integer page, Integer size, String sort);
     void addProduct(Product product);
     void updateProduct(Product product);
}
