package com.example.backend.controller.services;


import com.example.backend.model.User;
import com.example.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    UserRepository clientRepository;


    // register user.
    public void registerUserService(User user) {
        String passEncoded = passwordEncoder().encode(user.getPassword());
        // set password to bcrypt-encoded password.
        user.setPassword(passEncoded);
        // set role or level to always be "ROLE_CLIENT".
        user.setLevel("ROLE_CLIENT");
        // set status to true.
        user.setStatus(true);
        clientRepository.save(user);
    }

    public Optional<User> getClientProfile(String username) {
        return clientRepository.findByUsername(username);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
