package com.example.backend.controller.services;

import com.example.backend.controller.services.Productinterface.FilesStorageInterface;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Stream;
@Service
public class FilesStorageService implements FilesStorageInterface {
//    public static final String folderPath =  "img/";
//    public static final Path filePath = Paths.get(folderPath);
    private final Path root = Paths.get("img/");
    SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");

    @Override
    public void init() {
        try {
            System.out.println("Checking directory...");
            //create directory to save the files
            if (!Files.exists(root)) {
                System.out.println("Creating directory...");
                Files.createDirectories(root);
                System.out.println("Directory created.");
            }else{
                System.out.println("Directory already create.");
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    @Override
    public String save(MultipartFile file, String imgFileName, String time) {
        try {
            String imgName = imgFileName + "" + time + ".jpg";
            Files.copy(file.getInputStream(), this.root.resolve(imgName));
            return imgName;
         } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    @Override
    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = (Resource) new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(root.toFile());
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
        } catch (IOException e) {
            throw new RuntimeException("Could not load the files!");
        }
    }
}
