package com.example.backend.controller;

import com.example.backend.controller.services.ClientService;
import com.example.backend.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("/")
@CrossOrigin(origins = "http://localhost:3000")
public class ClientController {

    @Autowired
    ClientService clientService;

    // home page.
    @GetMapping()
    @ResponseBody
    public String home(){
        return "Welcome home!";
    }

    @GetMapping("/client")
    @ResponseBody
    public String client(){
        return "client";
    }

    // Register to application.
    @PostMapping("/register")
    public void registerUser(@RequestBody User user) {
        clientService.registerUserService(user);
    }

    @GetMapping("/client/profile")
    public Optional<User> profile(Authentication authentication) {
        return clientService.getClientProfile(authentication.getName());
    }
}
