package com.example.backend.controller;


import com.example.backend.controller.services.FilesStorageService;
import com.example.backend.controller.services.ProductService;
import com.example.backend.helper.CSVHelper;
import com.example.backend.message.ResponseMessage;
import com.example.backend.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = "http://localhost:3000")
public class ProductController {

    @Autowired
    ProductService productService;
    @Autowired
    FilesStorageService storageService;

    @PostMapping("/csv/upload")
    public ResponseEntity<ResponseMessage> uploadCsvFile(@RequestParam("file") MultipartFile file, @RequestParam("id") int admin_id) {
        String message = "";
        if (CSVHelper.hasCSVFormat(file)) {
            try {
                if(productService.save(file, admin_id, Date.valueOf(LocalDateTime.now().toLocalDate()))){
                    message = "Uploaded the file successfully: " + file.getOriginalFilename();
                }else{
                    message = "Could not upload the file1: " + file.getOriginalFilename() + "!";
                }
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
            } catch (Exception e) {
                message = "Could not upload the file2: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }
        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }

    @PostMapping("/img")
    public ResponseEntity<List<String>> imageProduct(@RequestParam("files") MultipartFile files, @RequestParam("productName") String productName
            , @RequestParam("category") String category, @RequestParam("brand") String brand, @RequestParam("size") String size
            , @RequestParam("color") String color, @RequestParam("time") String time) {
        List<String> fileNames = new ArrayList<>();
        try {
            AtomicInteger count = new AtomicInteger();
//            Arrays.asList(files).stream().forEach(file -> {
                fileNames.add(storageService.save(files, "pi" + count.getAndIncrement() + "_" + productService.generateSKU(productName, category, brand, size, color), time));
//            });
            System.out.println(fileNames);
//            System.out.println(storageService.load(fileNames.get(0)));
//            Arrays.asList(product).stream().forEach(file -> {
//                productD.add(file);
//            });
            return ResponseEntity.status(HttpStatus.OK).body(fileNames);
        } catch (NoSuchElementException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(fileNames);
        }
    }

    @PostMapping("/")
    public ResponseEntity<ResponseMessage> addProduct(@RequestBody Product product) {
        String productName = "";
        try {
            productName = product.getProductName();
            productService.addProduct(product);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(productName));
        } catch (NoSuchElementException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(productName));
        }
    }

    @PutMapping("/")
    public ResponseEntity<ResponseMessage> updateProduct(@RequestBody Product product) {
        String productName = "";
        try {
            System.out.println(product);
            productName = product.getProductName();
            productService.updateProduct(product);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(productName));
        } catch (NoSuchElementException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(productName));
        }
    }

    @GetMapping("/")
    public ResponseEntity<Page<Product>> productList(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        try {
            return new ResponseEntity<>(productService.getAllProduct(page, size), HttpStatus.OK);
        }catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/sort")
    public ResponseEntity<Page<Product>> productListSorted(@RequestParam("page") Integer page, @RequestParam("size") Integer size, @RequestParam("sort") String sort) {
        try {
            return new ResponseEntity<>(productService.getAllProductSorted(page, size, sort), HttpStatus.OK);
        }catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/{search}")
    public ResponseEntity<Slice<Product>> getProduct(@PathVariable("search") String search, @RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        try {
            Slice<Product> product = productService.searchProduct(search, page, size);
            return new ResponseEntity<>(product, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/sort/{search}")
    public ResponseEntity<Slice<Product>> getProductSorted(@PathVariable("search") String search, @RequestParam("page") Integer page, @RequestParam("size") Integer size, @RequestParam("sort") String sort) {
        try {
            Slice<Product> product = productService.searchProductSorted(search, page, size, sort);
            return new ResponseEntity<>(product, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/name/{search}")
    public ResponseEntity<Slice<Product>> getProductNameSorted(@PathVariable("search") String search, @RequestParam("page") Integer page, @RequestParam("size") Integer size, @RequestParam("sort") String sort) {
        try {
            Slice<Product> product = productService.getProductNameSorted(search, page, size, sort);
            return new ResponseEntity<>(product, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseMessage> deleteProduct(@PathVariable int id) {
        String productName = "";
        try {
            productName = productService.get(id).getProductName();
            productService.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(productName));
        } catch (NoSuchElementException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(productName));
        }
    }
}
