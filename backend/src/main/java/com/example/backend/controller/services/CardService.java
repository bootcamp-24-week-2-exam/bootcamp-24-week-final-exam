package com.example.backend.controller.services;

import com.example.backend.model.Card;
import com.example.backend.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CardService {

    @Autowired
    CardRepository cardRepository;
    public void addCard(Card card) {
        cardRepository.save(card);
    }

    public Optional<Card> getCard(Integer userid) {
        return cardRepository.findById(userid);
    }

}