package com.example.backend.controller.services;

import com.example.backend.controller.services.Transactioninterface.TransactionInterface;

import com.example.backend.dto.TransactionDto;
import com.example.backend.model.Transaction;
import com.example.backend.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class TransactionService implements TransactionInterface {

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public Integer save(Transaction transaction) {
        try {
            LocalDate currentDateTime = currentDateTime();
//            List<Transaction> transactionEntities = new ArrayList<>();
//
//            for (Transaction transaction1 : transaction) {
//                transactionEntities.add(dtoToEntity(transaction1, currentDateTime));
//            }
//            transactionRepository.saveAll(transactionEntities);

            transactionRepository.save(dtoToEntity(transaction, currentDateTime));
            System.out.println(getTrans(transaction.getUserid()));
            return getTrans(transaction.getUserid());
        } catch (Exception e) {
            System.out.println("failed transaction: " + e.getMessage());
            return 0;
        }
    }

//    @Override
//    public Boolean save(Transaction transaction) {
//        try {
//            LocalDate currentDateTime = currentDateTime();
////            List<Transaction> transactionEntities = new ArrayList<>();
////
////            for (Transaction transaction1 : transaction) {
////                transactionEntities.add(dtoToEntity(transaction1, currentDateTime));
////            }
////            transactionRepository.saveAll(transactionEntities);
//
//            transactionRepository.save(dtoToEntity(transaction, currentDateTime));
//            System.out.println(getTrans(transaction.getUserid()));
//            return true;
//        } catch (Exception e) {
//            System.out.println("failed transaction: " + e.getMessage());
//            return false;
//        }
//    }

    public Page<Transaction> getAllTransaction(Integer page, Integer size) {
        Page<Transaction> transactions = transactionRepository.findAll(PageRequest.of(page, size));
        return  transactions;
    }

//    public Page<TransactionDto> getAllByDate(Date min, Date max, Integer page, Integer size) {
//        Page<TransactionDto> transactions = transactionRepository.findByUpdatedatBetween(min, max, PageRequest.of(page, size));
//        return  transactions;
//    }

    public List<TransactionDto> getAllByDateExport(Date start, Date end) {
        List<TransactionDto> transactions = transactionRepository.getTransaction("2022-09-01", "2022-09-30");
//        List<TransactionDto> transactions = transactionRepository.getTransaction(start, end);
        return  transactions;
    }

    public List<Transaction> getAll() {
        List<Transaction> transactions = transactionRepository.findAll();
        return  transactions;
    }

    public static Transaction dtoToEntity(Transaction transaction, LocalDate currentDateTime){
        Transaction transactionEntity = new Transaction();
        transactionEntity.setUserid(transaction.getUserid());
        transactionEntity.setCardid(transaction.getCardid());
        transactionEntity.setAmount(transaction.getAmount());
        return transactionEntity;
    }

    public Integer getTrans(Integer user_id) {
        Integer tran = transactionRepository.getTransByUserId(user_id);
        return tran;
    }

    public LocalDate currentDateTime(){
//        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        return dateFormatter.format(new Date());
        return LocalDateTime.now().toLocalDate();
    }
}
