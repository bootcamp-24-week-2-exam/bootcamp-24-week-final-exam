package com.example.backend.controller.services.Cartinterface;

import com.example.backend.dto.CartProd;
import com.example.backend.model.Cart;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import java.util.List;


public interface CartInterface {
    Boolean save(Cart carts);
//    Slice<CartProd> getProductByCart(String name, Pageable pageable);
    Page<Cart> getAllCart(Integer page, Integer size);
    Slice<CartProd> fetchCartProdDataInnerJoin(String name, Integer page, Integer size);
    Integer getNewQuantity(String option, Integer cart_id);
    String getTotal(Integer cart_id, Integer quantity);
}
