package com.example.backend.controller.services.Productinterface;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface FilesStorageInterface {
    public void init();
    public String save(MultipartFile file, String imgFileName, String time);
    public Resource load(String filename);
    public void deleteAll();
    public Stream<Path> loadAll();
}
