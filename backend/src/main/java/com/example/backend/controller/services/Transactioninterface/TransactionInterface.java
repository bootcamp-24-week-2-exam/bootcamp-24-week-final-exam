package com.example.backend.controller.services.Transactioninterface;

import com.example.backend.dto.TransactionDto;
import com.example.backend.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface TransactionInterface {

//    Boolean save(List<Transaction> transaction);

    Integer save(Transaction transaction);

    Page<Transaction> getAllTransaction(Integer page, Integer size);
//    Page<TransactionDto> getAllByDate(Date min, Date max, Integer page, Integer size);
    List<TransactionDto> getAllByDateExport(Date start, Date end);
    Integer getTrans(Integer user_id);
}
