package com.example.backend.controller.services;

import com.example.backend.controller.services.Productinterface.ProductInterface;
import com.example.backend.helper.CSVHelper;
import com.example.backend.model.Product;
import com.example.backend.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class ProductService implements ProductInterface {

    @Autowired
    ProductRepository productRepository;

    public Boolean save(MultipartFile file, int admin_id, Date createdAt) {
        try {
            List<Product> products = CSVHelper.csvToProduct(file.getInputStream(), admin_id, createdAt, getLast());
            productRepository.saveAll(products);
            return true;
        } catch (IOException e) {
            System.out.println("fail to store csv data: " + e.getMessage());
            return false;
//            throw new RuntimeException("fail to store csv data: " + e.getMessage());

        }
    }
    public Page<Product> getAllProduct(Integer page, Integer size) {
        Page<Product> products = productRepository.findAll(PageRequest.of(page, size));
        return  products;
    }

    public Page<Product> getAllProductSorted(Integer page, Integer size, String sort) {
//        PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        Page<Product> products = null;
        if(sort.split(",")[1].equals("asc")){
            return  productRepository.findAll(PageRequest.of(page, size,
                    Sort.by(sort.split(",")[0]).ascending()));
        }else if (sort.split(",")[1].equals("desc")){
            return  productRepository.findAll(PageRequest.of(page, size,
                    Sort.by(sort.split(",")[0]).descending()));
        }

//        Page<Product> products = productRepository.findAll(PageRequest.of(page, size,
//                sort.split(",")[1] == "asc" ? Sort.by(sort.split(",")[0]).ascending() : Sort.by(sort.split(",")[0]).descending()));
        return  products;
    }

    public Product get(Integer id) {
        return productRepository.findById(id).get();
    }

    public Slice<Product> searchProduct(String search, Integer page, Integer size) {
        int id = 0;
        try{
            id = Integer.parseInt(search);
        }catch (Exception e){
            id = 0;
        }

        Pageable paging = PageRequest.of(page, size);
        Slice<Product> products = productRepository.findByProductNameContainingOrCategoryContainingOrBrandContainingOrColorContainingOrSizeContaining(search, search, search, search, search, paging);
        return products;
    }

    public Slice<Product> searchProductSorted(String search, Integer page, Integer size, String sort) {
        int id = 0;
        try{
            id = Integer.parseInt(search);
        }catch (Exception e){
            id = 0;
        }
        Page<Product> products = null;
        if(sort.split(",")[1].equals("asc")){
            return  productRepository.findByProductNameContainingOrCategoryContainingOrBrandContainingOrColorContainingOrSizeContaining(search, search, search, search, search,PageRequest.of(page, size,
                    Sort.by(sort.split(",")[0]).ascending()));
        }else if (sort.split(",")[1].equals("desc")){
            return  productRepository.findByProductNameContainingOrCategoryContainingOrBrandContainingOrColorContainingOrSizeContaining(search, search, search, search, search,PageRequest.of(page, size,
                    Sort.by(sort.split(",")[0]).descending()));
        }

//        Pageable paging = PageRequest.of(page, size, Sort.by(sort));
//        Slice<Product> products = productRepository.findByProductNameContainingOrCategoryContainingOrBrandContaining(search, search, search, paging);
        return products;
    }

    public Slice<Product> getProductNameSorted(String search, Integer page, Integer size, String sort) {
        int id = 0;
        try{
            id = Integer.parseInt(search);
        }catch (Exception e){
            id = 0;
        }
        Page<Product> products = null;
        if(sort.split(",")[1].equals("asc")){
            return  productRepository.findByProductNameContaining(search, PageRequest.of(page, size,
                    Sort.by(sort.split(",")[0]).ascending()));
        }else if (sort.split(",")[1].equals("desc")){
            return  productRepository.findByProductNameContaining(search, PageRequest.of(page, size,
                    Sort.by(sort.split(",")[0]).descending()));
        }

//        Pageable paging = PageRequest.of(page, size, Sort.by(sort));
//        Slice<Product> products = productRepository.findByProductNameContainingOrCategoryContainingOrBrandContaining(search, search, search, paging);
        return products;
    }

    public void deleteById(Integer id) {
            productRepository.deleteById(id);
    }

    public void addProduct(Product product) {
        product.setSKU(generateSKU(product.getProductName(), product.getCategory(),
                product.getBrand(), product.getSize(), product.getColor()));
        String imgName = "pi0_" + product.getSKU() + "" + product.getImage() + ".jpg";

//        product.setImage("pi0_" + imgName + "," + "pi1_" + imgName + "," + "pi2_" + imgName);
        product.setImage(imgName);
        productRepository.save(product);
    }

    public void updateProduct(Product product) {
        product.setSKU(generateSKU(product.getProductName(), product.getCategory(),
                product.getBrand(), product.getSize(), product.getColor()));
        String imgName = "pi0_" + product.getSKU() + "" + product.getImage() + ".jpg";
        if(!product.getImage().contains(".jpg")){
            product.setImage(imgName);
        }
        productRepository.save(product);
    }

    public Integer getLast() {
        Page<Product> products = productRepository.findAll(PageRequest.of(0, 10));
        return Math.toIntExact(products.getTotalElements());
    }

    public String generateSKU(String pname, String cat, String brand, String size, String Color){
        return "" + pname.substring(0, 3) + cat.substring(0, 2) + brand.substring(0, 2) + size.substring(0, 1) +
                Color.substring(0, 2);
    }
}
