package com.example.backend.controller;

import com.example.backend.controller.services.CardService;
import com.example.backend.dto.CartProd;
import com.example.backend.message.ResponseMessage;
import com.example.backend.model.Card;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/card")
@CrossOrigin(origins = "http://localhost:3000")

public class CardController {

    @Autowired
    CardService cardService;

    @PostMapping("")
    public ResponseEntity<ResponseMessage> addCard(@RequestBody Card card) {
        String cardName = "";
        try {
            cardName = card.getName();
            cardService.addCard(card);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(cardName));
        } catch (NoSuchElementException e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(cardName));
        }
    }

    @GetMapping("")
    public ResponseEntity<Optional<Card>> cardList(@RequestParam("userid") Integer userid) {
        try {
            return new ResponseEntity<>(cardService.getCard(userid), HttpStatus.OK);
        }catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}