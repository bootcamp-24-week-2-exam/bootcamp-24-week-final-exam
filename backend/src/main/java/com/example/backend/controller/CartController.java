package com.example.backend.controller;

import com.example.backend.controller.services.CartService;
import com.example.backend.dto.CartProd;
import com.example.backend.message.ResponseMessage;
import com.example.backend.model.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
@CrossOrigin(origins = "http://localhost:3000")
public class CartController {

    @Autowired
    CartService cartService;

    @PostMapping("")
    public ResponseEntity<ResponseMessage> saveCart(@RequestBody Cart cart) {
        if (cartService.save(cart))
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("Item save!"));
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Item not save!"));
    }

//    @GetMapping("")
//    public ResponseEntity<Page<Cart>> cartList(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
//        try {
//            return new ResponseEntity<>(cartService.getAllCart(page, size), HttpStatus.OK);
//        }catch (NullPointerException e) {
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//    }

    @GetMapping("/{search}")
    public ResponseEntity<Slice<CartProd>> cartList(@PathVariable("search") String search, @RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        try {
            return new ResponseEntity<>(cartService.fetchCartProdDataInnerJoin(search, page, size), HttpStatus.OK);
        }catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/{option}/{cart_id}")
    public ResponseEntity<Integer> updateQuantity(@PathVariable("option") String option, @PathVariable("cart_id") Integer cart_id) {
        try {
            return new ResponseEntity<>(cartService.getNewQuantity(option, cart_id), HttpStatus.OK);
        }catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/total/{cart_id}/{quantity}")
    public ResponseEntity<String> getTotal(@PathVariable("cart_id") Integer cart_id, @PathVariable("quantity") Integer quantity) {
        try {
            return new ResponseEntity<>(cartService.getTotal(cart_id, quantity), HttpStatus.OK);
        }catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/trans/{trans_id}")
    public ResponseEntity<Integer> updateQuantity(@PathVariable("trans_id") Integer trans_id, @RequestBody List<Integer> cartList) {
        try {
            Integer transactio_id = 0;
            System.out.println(trans_id);
            System.out.println(cartList);
            for (Integer cartid : cartList) {
                System.out.println(cartid);
                transactio_id = cartService.updateTransaction(trans_id, cartid);
            }
            return new ResponseEntity<>(transactio_id, HttpStatus.OK);
        }catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
