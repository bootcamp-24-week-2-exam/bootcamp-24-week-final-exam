package com.example.backend.controller;

import com.example.backend.controller.services.Logininterface.AuthRequest;
import com.example.backend.securityconfig.jwt.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
@CrossOrigin(origins = "http://localhost:3000")
public class AuthenticateController {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/authenticate")
    public String generateToken(@RequestBody AuthRequest authRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword())
            );
        } catch (Exception e) {
            throw new Exception("Invalid user/password");
        }

        // return generated username.
        return jwtUtil.generateToken(authRequest.getUsername());
    }

    // user role.
    @GetMapping("/user/role")
    public Object getUserRole(Authentication authentication) {
        return authentication.getAuthorities();
    }
}
