package com.example.backend.controller;


import com.example.backend.controller.services.AdminService;
import com.example.backend.model.User;
import com.example.backend.securityconfig.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "http://localhost:3000")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @GetMapping("/admin")
    @ResponseBody
    public String home(){
        return "admin";
    }
    @GetMapping("/admin/profile")
    public Optional<User> profile(Authentication authentication) {
        return adminService.getAdminProfile(authentication.getName());
    }

    @GetMapping("/admin/logout")
    public String admminLogout() {
        return "Logout successfully1";
    }

}
