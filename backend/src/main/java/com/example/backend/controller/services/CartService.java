package com.example.backend.controller.services;

import com.example.backend.controller.services.Cartinterface.CartInterface;
import com.example.backend.dto.CartProd;
import com.example.backend.model.Cart;
import com.example.backend.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartService implements CartInterface {
    @Autowired
    CartRepository cartRepository;

    public Boolean save(Cart cart) {
        try {
            cartRepository.save(cart);
            return true;
        } catch (Exception e) {
            System.out.println("fail to save data: " + e.getMessage());
            return false;
        }
    }

    public Page<Cart> getAllCart(Integer page, Integer size) {
        Page<Cart> carts = cartRepository.findAll(PageRequest.of(page, size));
        return carts;
    }

    public Slice<CartProd> fetchCartProdDataInnerJoin(String name, Integer page, Integer size) {
        System.out.println(cartRepository.getProductByCart(name, PageRequest.of(page, size)));
        Slice<CartProd> cartProds = cartRepository.getProductByCart(name, PageRequest.of(page, size));
        return cartProds;
    }

    public Integer getNewQuantity(String option, Integer cart_id) {
        Optional<Cart> cart = cartRepository.findById(cart_id);
        System.out.println(cart.get().getQuantity());
        Cart cart1 = new Cart(cart_id, cart.get().getUser_id(), cart.get().getProduct_id(), cart.get().getQuantity(),
                cart.get().getPrice(), cart.get().getStatus(), cart.get().getTransactionid());
        Integer newQuantity = 0;
        if(option.equals("add")){
            cart1.setQuantity(cart1.getQuantity() + 1);
            cartRepository.save(cart1);
            return cart1.getQuantity();
        }else if(option.equals("sub")){
            if (cart.get().getQuantity() > 1) {
                cart1.setQuantity(cart1.getQuantity() - 1);
                cartRepository.save(cart1);
                return cart1.getQuantity();
            }
        }
        return newQuantity;
    }

    public String getTotal(Integer cart_id, Integer quantity) {
        Optional<Cart> cart = cartRepository.findById(cart_id);
        System.out.println(cart.get().getProduct().getPrice() + ": " + (cart.get().getProduct().getPrice() * quantity));
        return ""+cart.get().getProduct().getPrice() * quantity;
    }

    public Integer updateTransaction(Integer trans_id, Integer cart_id) {
        Optional<Cart> cart = cartRepository.findById(cart_id);
        System.out.println(cart.get().getStatus());
        Cart cart1 = new Cart(cart_id, cart.get().getUser_id(), cart.get().getProduct_id(), cart.get().getQuantity(),
                cart.get().getPrice(), cart.get().getStatus(), cart.get().getTransactionid());

            cart1.setTransactionid(trans_id);
        cart1.setStatus(2);
            cartRepository.save(cart1);
            return cart1.getTransactionid();
    }
}
