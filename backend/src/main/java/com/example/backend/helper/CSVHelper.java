package com.example.backend.helper;

import com.example.backend.model.Product;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class CSVHelper {
    public static String TYPE = "text/csv";
    //static String[] HEADERs = { "Id", "Title", "Description", "Published" };
    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }
    public static List<Product> csvToProduct(InputStream is, int admin_id, Date createAt, int last) {
        last++;
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {
            List<Product> tutorials = new ArrayList<Product>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            for (CSVRecord csvRecord : csvRecords) {
                Product tutorial = new Product(
                        csvRecord.get("Product Name"),
                        csvRecord.get("Description"),
                        Integer.parseInt(csvRecord.get("Quanity")),
                        Double.parseDouble(csvRecord.get("Price")),
                        csvRecord.get("SKU") + "" + (last++),
                        csvRecord.get("Category"),
                        csvRecord.get("Brand"),
                        csvRecord.get("Size"),
                        csvRecord.get("Color"),
                        admin_id,
                        createAt
                );
                tutorials.add(tutorial);
            }
            return tutorials;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
