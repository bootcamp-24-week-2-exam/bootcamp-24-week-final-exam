package com.example.backend.helper;

import com.example.backend.dto.TransactionDto;
import com.example.backend.model.Transaction;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class PDFExporter {
    private List<TransactionDto> transactionList;

    public PDFExporter(List<TransactionDto> transactionList) {
        this.transactionList = transactionList;
    }

    private void writeTableHeader(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.BLUE);
        cell.setPadding(5);

        com.lowagie.text.Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.WHITE);

        cell.setPhrase(new Phrase("SKU", font));

        table.addCell(cell);

        cell.setPhrase(new Phrase("QUANTIY", font));
        cell.getHorizontalAlignment();
        table.addCell(cell);

//        cell.setPhrase(new Phrase("TOTAL AMOUNT", font));
//        table.addCell(cell);
    }

    private void writeTableData(PdfPTable table) {
        for (TransactionDto transaction : transactionList) {
            table.addCell(String.valueOf(transaction.getSku()));
            table.addCell(String.valueOf(transaction.getQuantity()));
//            table.addCell(String.valueOf(transaction.getAmount()));
        }
    }

    public void export(HttpServletResponse response, Date start, Date end) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();
        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(18);
        font.setColor(Color.BLACK);

        Paragraph p = new Paragraph("Total Number of Products Bought", font);
        p.setAlignment(Paragraph.ALIGN_CENTER);
        Paragraph p2 = new Paragraph(new SimpleDateFormat("yyyy-MM-dd").format(start) + " to " + new SimpleDateFormat("yyyy-MM-dd").format(end) , font);
        p2.setAlignment(Paragraph.ALIGN_CENTER);

        document.add(p);
        document.add(p2);
//        PdfPTable table = new PdfPTable(5);
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100f);
//        table.setWidths(new float[] {1.5f, 3.5f, 3.0f, 3.0f, 1.5f});
        table.setWidths(new float[] {3.5f, 2.0f});
        table.setSpacingBefore(10);

        writeTableHeader(table);
        writeTableData(table);

        document.add(table);

        document.close();

    }
}
