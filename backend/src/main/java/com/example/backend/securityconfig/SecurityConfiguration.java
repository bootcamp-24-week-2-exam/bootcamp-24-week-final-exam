package com.example.backend.securityconfig;


import com.example.backend.filter.JwtFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.userdetails.DaoAuthenticationConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;


@Configuration
@EnableWebSecurity
//@CrossOrigin("http://localhost:3000")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtFilter jwtFilter;

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService);
//    }


    // Authentication using DAO and bcrypt encryption for the password.
    @Bean
    public AuthenticationProvider authProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService);
        provider.setPasswordEncoder(new BCryptPasswordEncoder());
        return provider;
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    // Authorization roles permission
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
//                .antMatchers(HttpMethod.POST, "/product/csv/upload").hasAuthority("ROLE_ADMIN")
//                .antMatchers(HttpMethod.GET, "/product/").permitAll()
//                .antMatchers("/authenticateadmin").permitAll().anyRequest().authenticated()
//                .antMatchers(HttpMethod.GET, "/admin").hasAuthority("ROLE_ADMIN")
//                .antMatchers(HttpMethod.POST, "/register").hasAuthority("ROLE_CLIENT")
//                .antMatchers(HttpMethod.GET, "/").permitAll()
//                .and().httpBasic()
//                .and().formLogin();
                .antMatchers(HttpMethod.GET, "/").permitAll()
                .antMatchers(HttpMethod.GET, "/admin").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.GET, "/admin/profile").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.POST, "/product/csv/upload").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.GET, "/client").hasAuthority("ROLE_CLIENT")
                .antMatchers(HttpMethod.GET, "/client/profile").hasAuthority("ROLE_CLIENT")
                .antMatchers(HttpMethod.POST, "/card").permitAll()
                .antMatchers(HttpMethod.GET, "/card").permitAll()
                .antMatchers(HttpMethod.POST, "/product/csv/upload").permitAll()
                .antMatchers(HttpMethod.POST, "/product/img").permitAll()
                .antMatchers(HttpMethod.GET, "/product/**").permitAll()
                .antMatchers(HttpMethod.POST, "/product/").permitAll()
                .antMatchers(HttpMethod.PUT, "/product/").permitAll()
                .antMatchers(HttpMethod.DELETE, "/product/**").permitAll()
                .antMatchers(HttpMethod.GET, "/user/role").permitAll()
                .antMatchers(HttpMethod.POST, "/register").permitAll()
                .antMatchers(HttpMethod.GET, "/product/sort").permitAll()
                .antMatchers(HttpMethod.GET, "/product/sort/**").permitAll()
                .antMatchers(HttpMethod.GET, "/product/name/**").permitAll()
                .antMatchers(HttpMethod.POST, "/transaction/trans/**").permitAll()
                .antMatchers(HttpMethod.GET, "/transaction").permitAll()
                .antMatchers(HttpMethod.GET, "/transaction/date").permitAll()
                .antMatchers(HttpMethod.GET, "/transaction/date/pdf").permitAll()
                .antMatchers(HttpMethod.POST, "/cart").permitAll()
                .antMatchers(HttpMethod.GET, "/cart").permitAll()
                .antMatchers(HttpMethod.GET, "/cart/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/cart/add/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/cart/sub/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/cart/total/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/cart/trans/**").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("/authenticate").permitAll().anyRequest().authenticated()
                .and()
                .exceptionHandling()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }


//    @Bean
//    public PasswordEncoder getPasswordEncoder() {
//        return new B;
//    }

}

