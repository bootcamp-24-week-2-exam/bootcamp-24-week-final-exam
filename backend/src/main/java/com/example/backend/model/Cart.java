package com.example.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "cart")
@Data
@AllArgsConstructor
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cart_id")
    private Integer cart_id;

    @Column(name = "user_id", nullable = false)
    private Integer user_id;

    @Column(name = "product_id", nullable = false)
    private Integer product_id;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @Column(name = "price", nullable = false)
    private double price;

    @Column(name = "status")
    private Integer status;

    @Column(name = "transaction_id", insertable = true, nullable = true)
    private Integer transactionid;

    @OneToOne(targetEntity=Product.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id", referencedColumnName = "id", nullable = false, updatable = false, insertable = false)
    private Product product;

    public Cart(){

    }

    public Cart(Integer cart_id, Integer user_id, Integer product_id, Integer quantity, double price, Integer status, Integer transactionid) {
        this.cart_id = cart_id;
        this.user_id = user_id;
        this.product_id = product_id;
        this.quantity = quantity;
        this.price = price;
        this.status = status;
        this.transactionid = transactionid;
    }
}
