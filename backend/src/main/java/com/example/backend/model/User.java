package com.example.backend.model;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table (name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column (name = "id", length = 11)
    private Integer id;

    @Column (name = "firstname", nullable = false)
    private String firstname;

    @Column (name = "lastname", nullable = false)
    private String lastname;

    @Column (name = "middlename", nullable = false)
    private String middlename;

    @Column (name = "suffix")
    private String suffix;

    @Column (name = "gender", nullable = false)
    private String gender;

    @Column (name = "mobileno", nullable = false)
    private String mobileno;

    @Column (name = "email", nullable = false)
    private String email;

    @Column (name = "address", nullable = false)
    private String address;

    @Column (name = "username", nullable = false)
    private String username;

    @Column (name = "password", nullable = false)
    private String password;

    @Column (name = "level", nullable = false)
    private String level;

    @Column (name = "status", nullable = false)
    private boolean status;

    @OneToMany(targetEntity=Cart.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false, updatable = false, insertable = false)
    private List<Cart> cartList;

    @OneToMany(targetEntity=Card.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false, updatable = false, insertable = false)
    private List<Card> cardList;

//    @Column(name = "card_id", nullable = false)
//    private Integer cardId;
//
//
//    //Constructor for Card ID
//    public User(Integer cardId) {
//        this.cardId = cardId;
//    }
//
//    public void setCardId(Integer cardId) {
//        this.cardId = cardId;
//    }

    // Constructor
    public User(Integer id, String firstname, String lastname, String middlename, String suffix, String gender, String mobileno, String email, String address, String username, String password, String level, boolean status) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.suffix = suffix;
        this.gender = gender;
        this.mobileno = mobileno;
        this.email = email;
        this.address = address;
        this.username = username;
        this.password = password;
        this.level = level;
        this.status = status;
    }

    // Empty constructor
    public User() {
    }


    // Getters and Setters.
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
    
}
