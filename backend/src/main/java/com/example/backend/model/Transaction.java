package com.example.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "transaction")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private Integer transactionid;

    @Column(name = "user_id")
    private Integer userid;

    @Column(name = "amount", nullable = false)
    private double amount;

    @Column(name = "card_id")
    private Integer cardid;

    @Column(name = "updated_at")
    private Integer updatedat;

    public Transaction(Integer userid, double amount, Integer cardid) {
        this.userid = userid;
        this.amount = amount;
        this.cardid = cardid;
    }
}
