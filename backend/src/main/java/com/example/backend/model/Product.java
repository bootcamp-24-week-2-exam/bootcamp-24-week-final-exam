package com.example.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "product")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer productID;

    @Column(name = "name", nullable = false)
    private String productName;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @Column(name = "price", nullable = false)
    private double price;

    @Column(name = "sku")
    private String SKU;

    @Column(name = "rating")
    private int rating;

    @Column(name = "image")
    private String image;

    @Column(name = "category")
    private String category;

    @Column(name = "brand")
    private String brand;

    @Column(name = "size")
    private String size;

    @Column(name = "color")
    private String color;

    @Column(name = "admin_id")
    private Integer admin_id;

    @Column(name = "status")
    private String status;

    @Column(name = "created_at")
    private Date createdAt;

    public Product(String productName, String desciption, int quantity, double price, String SKU,
                   String category, String brand, String size, String color, int admin_id, Date createdAt) {
        this.productName = productName;
        this.description = desciption;
        this.quantity = quantity;
        this.price = price;
        this.SKU = SKU;
        this.category = category;
        this.brand = brand;
        this.size = size;
        this.color = color;
        this.admin_id = admin_id;
        this.createdAt = createdAt;
    }

    public Product(String image, String productName, String desciption, int quantity, double price, String SKU,
                   String category, String brand, String size, String color, int admin_id, Date createdAt) {
        this.image = image;
        this.productName = productName;
        this.description = desciption;
        this.quantity = quantity;
        this.price = price;
        this.SKU = SKU;
        this.category = category;
        this.brand = brand;
        this.size = size;
        this.color = color;
        this.admin_id = admin_id;
        this.createdAt = createdAt;
    }
}
