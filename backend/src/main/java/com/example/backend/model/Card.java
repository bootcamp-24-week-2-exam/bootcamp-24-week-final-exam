package com.example.backend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Table(name = "card")
@Entity
@Data
@NoArgsConstructor
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "number", nullable = false)
    private String number;

    @Column(name = "cardtype_id", nullable = false)
    private Integer cardtypeid;

    @Column(name = "cvc",nullable = false)
    private Integer cvc;

    @Column(name = "expiry", nullable = false)
        private String expiry;

    @Column(name = "balance", nullable = true)
    private Double balance;

    @Column(name = "user_id", nullable = false)
    private Integer userid;

    @OneToMany(targetEntity=CardType.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "cardtype_id", referencedColumnName = "id", nullable = false, updatable = false, insertable = false)
    private List<CardType> cardType;

    public Card(int id, String name, String number, Integer cardtypeid, Integer cvc, String expiry, Double balance, Integer userid) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.cardtypeid = cardtypeid;
        this.cvc = cvc;
        this.expiry = expiry;
        this.balance = balance;
        this.userid = userid;
    }
}