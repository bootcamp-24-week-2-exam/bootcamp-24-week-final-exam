package com.example.backend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Table(name = "cardtype")
@Entity
@Data
@NoArgsConstructor
public class CardType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cardtype_id")
    private Integer cardtype_id;

    @Column(name = "name", nullable = false)
    private String name;
}
