package com.example.backend.repository;

import com.example.backend.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {
//    Slice<Product> findByProductName(String search, Pageable pageable);
    Slice<Product> findByProductNameContainingOrCategoryContainingOrBrandContainingOrColorContainingOrSizeContaining(String name, String category, String brand, String color, String size, Pageable pageable);
    Slice<Product> findByProductNameContaining(String name, Pageable pageable);

//    @Query("SELECT p FROM product p WHERE p.name LIKE %:search% or p.brand Like %:search%")
//    Slice<Product> searchProduct(String search, Pageable pageable);
        Integer findTopByOrderByProductIDDesc();
}
