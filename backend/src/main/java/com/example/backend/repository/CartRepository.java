package com.example.backend.repository;

import com.example.backend.dto.CartProd;
import com.example.backend.model.Cart;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CartRepository extends PagingAndSortingRepository<Cart, Integer> {
//    @Query("SELECT new CartProd(c.cart_id, c.product_id, p.name, p.quantity, p.price) "
//            + "FROM cart c INNER JOIN c.product p WHERE c.product_id = p.id")
//    List<CartProd> fetchCartProdDataInnerJoin();

    @Query("SELECT new com.example.backend.dto.CartProd(c.cart_id, c.product_id, p.productName, p.quantity, p.price) FROM Cart c, Product p WHERE c.product_id = p.productID and p.productName LIKE %:name%")
    Slice<CartProd> getProductByCart(String name, Pageable pageable);

//    @Query("SELECT new c.quantity FROM Cart c WHERE c.cart_id = :cartid")
//    Integer getQuantity(Integer cartid);

    @Modifying
    @Query("update Cart c set c.quantity = ?1 where c.cart_id = ?2")
    void setQuantity(Integer quantity, Integer cartid);
}
