package com.example.backend.repository;

import com.example.backend.dto.CartProd;
import com.example.backend.dto.TransactionDto;
import com.example.backend.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
//    Page<TransactionDto> findByUpdatedatBetween(Date min, Date max, Pageable pageable);
    List<TransactionDto> findByUpdatedatBetween(Date min, Date max);

    @Query("SELECT new com.example.backend.dto.TransactionDto(p.SKU, SUM(c.quantity), SUM(c.price)) FROM Cart c, Product p, Transaction t WHERE c.product_id = p.id and c.transactionid = t.transactionid GROUP BY p.SKU")
    Page<TransactionDto> getTransaction(Date min, Date max, Pageable pageable);

    @Query("SELECT new com.example.backend.dto.TransactionDto(p.SKU, SUM(c.quantity), SUM(c.price)) FROM Cart c, Product p, Transaction t WHERE c.product_id = p.id and c.transactionid = t.transactionid GROUP BY p.SKU")
    List<TransactionDto> getTransaction(String min, String max);

//    @Query("SELECT new com.example.backend.dto.TransactionDto(p.SKU, SUM(c.quantity), SUM(c.price)) FROM Cart c, Product p, Transaction t WHERE t.updatedat BETWEEN :min AND :max and c.product_id = p.id and c.transactionid = t.transactionid GROUP BY p.SKU")
//    List<TransactionDto> getTransaction(String min, String max);

    @Query(
            value = "SELECT transaction_id FROM transaction WHERE user_id = ?1 ORDER BY transaction_id DESC LIMIT 1",
//            countQuery = "SELECT count(*) FROM Users",
            nativeQuery = true)
    Integer getTransByUserId(Integer userid);
}
