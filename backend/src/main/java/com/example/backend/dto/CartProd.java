package com.example.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartProd {
    private int cart_id;
    private int product_id;
    private String product_name;
    private int quantity;
    private double price;
}
