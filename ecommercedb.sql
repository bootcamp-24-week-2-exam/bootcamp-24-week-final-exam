-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: ecommercedb
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `card`
--

DROP TABLE IF EXISTS `card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `card` (
  `id` int NOT NULL AUTO_INCREMENT,
  `balance` double DEFAULT NULL,
  `cvc` int NOT NULL,
  `expiry` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `number` int NOT NULL,
  `cardtype_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_card_user_idx` (`user_id`),
  KEY `fk_card_cardtype_idx` (`cardtype_id`),
  CONSTRAINT `fk_card_cardtype` FOREIGN KEY (`cardtype_id`) REFERENCES `cardtype` (`cardtype_id`),
  CONSTRAINT `fk_card_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card`
--

LOCK TABLES `card` WRITE;
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
/*!40000 ALTER TABLE `card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardtype`
--

DROP TABLE IF EXISTS `cardtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cardtype` (
  `cardtype_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cardtype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardtype`
--

LOCK TABLES `cardtype` WRITE;
/*!40000 ALTER TABLE `cardtype` DISABLE KEYS */;
INSERT INTO `cardtype` VALUES (1,'Debit','2022-09-16 03:49:52'),(2,'Credit','2022-09-16 03:49:52');
/*!40000 ALTER TABLE `cardtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart` (
  `cart_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `quantity` int NOT NULL,
  `status` int DEFAULT NULL,
  `transaction_id` int DEFAULT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `fk_cart_user_idx` (`user_id`),
  KEY `fk_cart_product_idx` (`product_id`),
  KEY `fk_cart_transaction_idx` (`transaction_id`),
  CONSTRAINT `fk_cart_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `fk_cart_transaction` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`),
  CONSTRAINT `fk_cart_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (1,1,1,1,1,8,500),(2,1,2,1,1,8,300),(3,1,3,1,1,9,500);
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (13);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(256) NOT NULL,
  `quantity` int NOT NULL,
  `price` double NOT NULL,
  `sku` varchar(45) DEFAULT NULL,
  `rating` int DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  `brand` varchar(45) DEFAULT NULL,
  `size` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `admin_id` int NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Bench V neck black','Bench V neck t-shirt black',22,500,'Bent-BeLBl',0,'pi0_Bent-BeLBl1663167595271.jpg','t-shirt','Bench','L','Black',1,NULL,'2022-09-16 03:57:01',NULL),(2,'Bench Round neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',5,800,'Ben-T2',0,NULL,'Top','Bench','S','Red',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(3,'Bench V neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',10,800,'Ben-T3',0,NULL,'Top','Bench','L','White',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(4,'Bench Round neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',5,800,'Ben-T4',0,NULL,'Top','Bench','XL','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(5,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',12,1000,'RRJ-T5',0,NULL,'Top','RRJ','M','White',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(6,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',8,1000,'RRJ-T6',0,NULL,'Top','RRJ','L','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(7,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',4,1000,'RRJ-T7',0,NULL,'Top','RRJ','XL','Red',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(8,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',3,750,'BNY-T8',0,NULL,'Top','BNY','S','White',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(9,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',16,750,'BNY-T9',0,NULL,'Top','BNY','M','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(10,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',20,750,'BNY-T10',0,NULL,'Top','BNY','L','Red',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(11,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',18,750,'BNY-T11',0,NULL,'Top','BNY','XL','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(12,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1200,'Fubu-B12',0,NULL,'Bottom','Fubu','S','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(13,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',12,1200,'Fubu-B13',0,NULL,'Bottom','Fubu','M','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(14,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1200,'Fubu-B14',0,NULL,'Bottom','Fubu','L','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(15,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1200,'Fubu-B15',0,NULL,'Bottom','Fubu','XL','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(16,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',5,1200,'Fubu-B16',0,NULL,'Bottom','Fubu','M','White',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(17,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',3,1200,'Fubu-B17',0,NULL,'Bottom','Fubu','M','Red',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(18,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',5,1500,'JAG-B18',0,NULL,'Bottom','JAG','S','White',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(19,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1500,'JAG-B19',0,NULL,'Bottom','JAG','M','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(20,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1500,'JAG-B20',0,NULL,'Bottom','JAG','L','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(21,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',12,1500,'JAG-B21',0,NULL,'Bottom','JAG','XL','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(22,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',8,1000,'LEE-B22',0,NULL,'Bottom','LEE','M','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(23,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',8,1000,'LEE-B23',0,NULL,'Bottom','LEE','L','White',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(24,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1000,'LEE-B24',0,NULL,'Bottom','LEE','L','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(25,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1000,'LEE-B25',0,NULL,'Bottom','LEE','XL','Black',1,NULL,'2022-09-14 15:51:40','2022-09-14'),(26,'Bench Round neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',5,800,'Ben-T26',0,NULL,'Top','Bench','S','Red',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(27,'Bench V neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',10,800,'Ben-T27',0,NULL,'Top','Bench','L','White',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(28,'Bench Round neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',5,800,'Ben-T28',0,NULL,'Top','Bench','XL','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(29,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',12,1000,'RRJ-T29',0,NULL,'Top','RRJ','M','White',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(30,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',8,1000,'RRJ-T30',0,NULL,'Top','RRJ','L','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(31,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',4,1000,'RRJ-T31',0,NULL,'Top','RRJ','XL','Red',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(32,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',3,750,'BNY-T32',0,NULL,'Top','BNY','S','White',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(33,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',16,750,'BNY-T33',0,NULL,'Top','BNY','M','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(34,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',20,750,'BNY-T34',0,NULL,'Top','BNY','L','Red',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(35,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',18,750,'BNY-T35',0,NULL,'Top','BNY','XL','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(36,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1200,'Fubu-B36',0,NULL,'Bottom','Fubu','S','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(37,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',12,1200,'Fubu-B37',0,NULL,'Bottom','Fubu','M','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(38,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1200,'Fubu-B38',0,NULL,'Bottom','Fubu','L','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(39,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1200,'Fubu-B39',0,NULL,'Bottom','Fubu','XL','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(40,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',5,1200,'Fubu-B40',0,NULL,'Bottom','Fubu','M','White',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(41,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',3,1200,'Fubu-B41',0,NULL,'Bottom','Fubu','M','Red',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(42,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',5,1500,'JAG-B42',0,NULL,'Bottom','JAG','S','White',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(43,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1500,'JAG-B43',0,NULL,'Bottom','JAG','M','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(44,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1500,'JAG-B44',0,NULL,'Bottom','JAG','L','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(45,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',12,1500,'JAG-B45',0,NULL,'Bottom','JAG','XL','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(46,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',8,1000,'LEE-B46',0,NULL,'Bottom','LEE','M','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(47,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',8,1000,'LEE-B47',0,NULL,'Bottom','LEE','L','White',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(48,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1000,'LEE-B48',0,NULL,'Bottom','LEE','L','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(49,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1000,'LEE-B49',0,NULL,'Bottom','LEE','XL','Black',1,NULL,'2022-09-14 20:35:03','2022-09-15'),(54,'Bench Round neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',5,800,'Ben-T51',0,NULL,'Top','Bench','S','Red',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(55,'Bench V neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',10,800,'Ben-T52',0,NULL,'Top','Bench','L','White',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(56,'Bench Round neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',5,800,'Ben-T53',0,NULL,'Top','Bench','XL','Black',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(57,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',12,1000,'RRJ-T54',0,NULL,'Top','RRJ','M','White',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(58,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',8,1000,'RRJ-T55',0,NULL,'Top','RRJ','L','Black',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(59,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',4,1000,'RRJ-T56',0,NULL,'Top','RRJ','XL','Red',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(60,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',3,750,'BNY-T57',0,NULL,'Top','BNY','S','White',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(61,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',16,750,'BNY-T58',0,NULL,'Top','BNY','M','Black',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(62,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',20,750,'BNY-T59',0,NULL,'Top','BNY','L','Red',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(63,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',18,750,'BNY-T60',0,NULL,'Top','BNY','XL','Black',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(64,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1200,'Fubu-B61',0,NULL,'Bottom','Fubu','S','Black',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(65,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',12,1200,'Fubu-B62',0,NULL,'Bottom','Fubu','M','Black',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(66,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1200,'Fubu-B63',0,NULL,'Bottom','Fubu','L','Black',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(67,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1200,'Fubu-B64',0,NULL,'Bottom','Fubu','XL','Black',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(68,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',5,1200,'Fubu-B65',0,NULL,'Bottom','Fubu','M','White',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(69,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',3,1200,'Fubu-B66',0,NULL,'Bottom','Fubu','M','Red',1,NULL,'2022-09-15 16:45:34','2022-09-16'),(70,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',5,1500,'JAG-B67',0,NULL,'Bottom','JAG','S','White',1,NULL,'2022-09-15 16:45:35','2022-09-16'),(71,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1500,'JAG-B68',0,NULL,'Bottom','JAG','M','Black',1,NULL,'2022-09-15 16:45:35','2022-09-16'),(72,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1500,'JAG-B69',0,NULL,'Bottom','JAG','L','Black',1,NULL,'2022-09-15 16:45:35','2022-09-16'),(73,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',12,1500,'JAG-B70',0,NULL,'Bottom','JAG','XL','Black',1,NULL,'2022-09-15 16:45:35','2022-09-16'),(74,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',8,1000,'LEE-B71',0,NULL,'Bottom','LEE','M','Black',1,NULL,'2022-09-15 16:45:35','2022-09-16'),(75,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',8,1000,'LEE-B72',0,NULL,'Bottom','LEE','L','White',1,NULL,'2022-09-15 16:45:35','2022-09-16'),(76,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1000,'LEE-B73',0,NULL,'Bottom','LEE','L','Black',1,NULL,'2022-09-15 16:45:35','2022-09-16'),(77,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1000,'LEE-B74',0,NULL,'Bottom','LEE','XL','Black',1,NULL,'2022-09-15 16:45:35','2022-09-16'),(79,'Bench Round neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',5,800,'Ben-T74',0,NULL,'Top','Bench','S','Red',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(80,'Bench V neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',10,800,'Ben-T75',0,NULL,'Top','Bench','L','White',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(81,'Bench Round neck t-shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',5,800,'Ben-T76',0,NULL,'Top','Bench','XL','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(82,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',12,1000,'RRJ-T77',0,NULL,'Top','RRJ','M','White',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(83,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',8,1000,'RRJ-T78',0,NULL,'Top','RRJ','L','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(84,'RRJ Poloshirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',4,1000,'RRJ-T79',0,NULL,'Top','RRJ','XL','Red',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(85,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',3,750,'BNY-T80',0,NULL,'Top','BNY','S','White',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(86,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',16,750,'BNY-T81',0,NULL,'Top','BNY','M','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(87,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',20,750,'BNY-T82',0,NULL,'Top','BNY','L','Red',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(88,'BNY Muscle shirt','Made from 100% organic cotton. This super soft tagless shirt offers a trim but comfortable fit while on the move',18,750,'BNY-T83',0,NULL,'Top','BNY','XL','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(89,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1200,'Fubu-B84',0,NULL,'Bottom','Fubu','S','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(90,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',12,1200,'Fubu-B85',0,NULL,'Bottom','Fubu','M','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(91,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1200,'Fubu-B86',0,NULL,'Bottom','Fubu','L','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(92,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1200,'Fubu-B87',0,NULL,'Bottom','Fubu','XL','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(93,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',5,1200,'Fubu-B88',0,NULL,'Bottom','Fubu','M','White',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(94,'Fubu Ragid Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',3,1200,'Fubu-B89',0,NULL,'Bottom','Fubu','M','Red',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(95,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',5,1500,'JAG-B90',0,NULL,'Bottom','JAG','S','White',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(96,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1500,'JAG-B91',0,NULL,'Bottom','JAG','M','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(97,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',15,1500,'JAG-B92',0,NULL,'Bottom','JAG','L','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(98,'JAG Straignt Cut Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',12,1500,'JAG-B93',0,NULL,'Bottom','JAG','XL','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(99,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',8,1000,'LEE-B94',0,NULL,'Bottom','LEE','M','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(100,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',8,1000,'LEE-B95',0,NULL,'Bottom','LEE','L','White',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(101,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1000,'LEE-B96',0,NULL,'Bottom','LEE','L','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16'),(102,'Lee Jogger Pants','These cotton and spandex blend pants have reinforcement, stretch, and adjustability in all the right areas that make them ideal for any expedition big or small.',10,1000,'LEE-B97',0,NULL,'Bottom','LEE','XL','Black',1,NULL,'2022-09-15 22:56:16','2022-09-16');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction` (
  `transaction_id` int NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL,
  `card_id` int NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`transaction_id`),
  KEY `frk_transaction_card_idx` (`card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (8,800,1,'2022-09-16 06:32:37'),(9,500,2,'2022-09-16 06:32:37');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `middlename` varchar(45) NOT NULL,
  `suffix` varchar(45) DEFAULT NULL,
  `gender` varchar(45) NOT NULL,
  `mobileno` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `address` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` text NOT NULL,
  `level` varchar(45) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `mobileno_UNIQUE` (`mobileno`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Samuel','Guban','b',NULL,'M','09123456789','samuel.guban@novare.com.hk','bicol','sam','$2a$10$6f5AHrEk3h4d1f12K0EKCuOP2svdbHypET9rqpr1KJY/69tfkuSbe','ROLE_ADMIN','1'),(10,'Samuel','Guban','B',NULL,'M','09321654789','samuelguban@novare.com.hk','Camarines Norte','samclient','$2a$10$gpLLaIkW8qXiHvgX1ga4ouUbQ1XYozrDnEo712xVFLylykzi4/KJW','ROLE_CLIENT','1');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-16 16:08:28
