// import logo from "./logo.svg";
import "./App.css";
import Home from "./public/Home";
import Register from "./public/Register";
import Login from "./public/Login";
import About from "./public/About";
import Contact from "./public/Contact";
import Logout from "./components/Logout";
import Authenticate from "./auth/Authenticate";
import Products from "./pages/Products";
import AdminProfile from "./pages/AdminProfile";
import ClientProfile from "./pages/ClientProfile";
import Cart from "./pages/Cart";
import PageNotFound from "./Errors/pageNotFound";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import UserContext from "./context/UserContext";
import { useState } from "react";
import ProductOverView from "./components/Products/ProductOverView";

function App() {
  // implement context .
  const [userName, setUserName] = useState("");
  const [passWord, setPassWord] = useState("");
  const [Jwt, setJwt] = useState("");
  const [user, setUser] = useState([]);
  const [LoggedIn, setLoggedIn] = useState("");
  const [role, setRole] = useState("");

  // console.log(user.firstname);

  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}

      <UserContext.Provider
        value={{
          userName,
          setUserName,
          passWord,
          setPassWord,
          Jwt,
          setJwt,
          user,
          setUser,
          LoggedIn,
          setLoggedIn,
          role,
          setRole,
        }}
      >
        {/* routes */}

        <Router>
          <Routes>
            <Route path="/" exact element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/about" element={<About />} />
            <Route path="/contact" element={<Contact />} />
            <Route path="/authenticate" element={<Authenticate />} />
            <Route path="/admin/profile" element={<AdminProfile />} />
            <Route path="/admin/products" element={<Products />} />
            <Route path="/client/profile" element={<ClientProfile />} />
            <Route path="/client/cart" element={<Cart />} />
            <Route path="/product/overview" element={<ProductOverView />} />
            <Route path="*" element={<PageNotFound />} />
          </Routes>
        </Router>
      </UserContext.Provider>
    </div>
  );
}

export default App;
