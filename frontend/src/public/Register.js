import React, { useContext, useState } from "react";
import { Button, Form } from "react-bootstrap";
import UserContext from "../context/UserContext";
import axios from "../api/axios";
import Success from "../alerts/Success";
import Error from "../alerts/Error";
// import { useNavigate } from "react-router-dom";

const Register = () => {
  // navigation.
  // const navigate = useNavigate();

  const { userName, setUserName, passWord, setPassWord } =
    useContext(UserContext);
  const [firstname, setFirstName] = useState("");
  const [middlename, setMiddleName] = useState("");
  const [lastname, setLastName] = useState("");
  const [suffix, setSuffix] = useState(null);
  const [gender, setGender] = useState("");
  const [mobileno, setMobileNo] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");

  // alert info
  const [headerSuccess, setHeaderSuccess] = useState("");
  const [bodySuccess, setBodySuccess] = useState("");
  const [notifySuccess, setNotifySuccess] = useState(false);
  const [headerError, setHeaderError] = useState("");
  const [bodyError, setBodyError] = useState("");
  const [notifyError, setNotifyError] = useState(false);

  const handleFirstname = (e) => {
    setFirstName(e.target.value);
  };

  const handleMiddlename = (e) => {
    setMiddleName(e.target.value);
  };

  const handleLastname = (e) => {
    setLastName(e.target.value);
  };

  const handleSuffix = (e) => {
    setSuffix(e.target.value);
  };

  const handleGender = (e) => {
    setGender(e.target.value);
  };

  const handleMobileno = (e) => {
    setMobileNo(e.target.value);
  };

  const handleEmail = (e) => {
    setEmail(e.target.value);
  };

  const handleAddress = (e) => {
    setAddress(e.target.value);
  };

  const handleUsername = (e) => {
    setUserName(e.target.value);
  };

  const handlePassword = (e) => {
    setPassWord(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    let newUser = {
      firstname: firstname,
      lastname: lastname,
      middlename: middlename,
      suffix: suffix,
      gender: gender,
      mobileno: mobileno,
      email: email,
      address: address,
      username: userName,
      password: passWord,
    };

    console.log(newUser);

    // register using post.
    // authenticate and set Jwt response and set Login to true..
    axios
      .post("/register", newUser)
      .then((response) => {
        console.log("Successful Login" + response);
        // set alert to display.
        alertFunction(true);
      })
      .catch((error) => {
        console.log(error);
        alertFunction(false);
      });
  };

  // alert redirection.
  const alertFunction = (notif) => {
    if (notif) {
      setHeaderSuccess("Registration success!");
      setBodySuccess("You have Successfully registered for an account!");
      setNotifySuccess(true);

      // redirect to login page.
      // navigate("/login");
    } else {
      setHeaderError("Registration failed!");
      setBodyError(
        "Your registration attempt failed. Please recheck your inputted credentials and try again"
      );
      setNotifyError(true);
    }
  };

  return (
    <div className="Register">
      <h3>Register user page</h3>

      {/* notification */}
      <Success
        header={headerSuccess}
        body={bodySuccess}
        notify={notifySuccess}
      />
      {notifySuccess && (
        <Button
          variant="primary"
          onClick={(e) => (window.location.href = "/login")}
        >
          Go To Login
        </Button>
      )}
      <Error header={headerError} body={bodyError} notify={notifyError} />

      <Form className="form-style" onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="formBasicfirstname">
          <Form.Label>First name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter firstname"
            required
            value={firstname}
            onChange={handleFirstname}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicMiddlename">
          <Form.Label>Middle name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter middle ame"
            required
            value={middlename}
            onChange={handleMiddlename}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicLastname">
          <Form.Label>Last name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Lastname"
            required
            value={lastname}
            onChange={handleLastname}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Suffix</Form.Label>
          <Form.Control
            type="text"
            placeholder="Example: Jr. Sr. etc.."
            value={suffix}
            onChange={handleSuffix}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicGender">
          <Form.Label>Gender</Form.Label>
          <Form.Select
            aria-label="Default select example"
            value={gender}
            onChange={handleGender}
          >
            <option>select gender</option>
            <option value="M">Male</option>
            <option value="F">Female</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicMobileno">
          <Form.Label>Mobile no.</Form.Label>
          <Form.Control
            type="text"
            placeholder="ex: 09978495571"
            required
            value={mobileno}
            onChange={handleMobileno}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            required
            value={email}
            onChange={handleEmail}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicAddress">
          <Form.Label>Address</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter complete address"
            required
            value={address}
            onChange={handleAddress}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicUsername">
          <Form.Label>Username</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter username (mus be unique)"
            required
            value={userName}
            onChange={handleUsername}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter password"
            required
            value={passWord}
            onChange={handlePassword}
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    </div>
  );
};

export default Register;
