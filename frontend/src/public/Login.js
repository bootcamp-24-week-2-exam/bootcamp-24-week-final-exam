import React, { useContext, useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import UserContext from "../context/UserContext";
import axios from "../api/axios";
import Success from "../alerts/Success";
import Error from "../alerts/Error";


const Login = () => {

    // user context info.
    const { userName, setUserName, passWord, setPassWord, Jwt, setJwt, LoggedIn, setLoggedIn } = useContext(UserContext);

    // navigate to page
    const navigate = useNavigate();

    // alert info
    const [headerSuccess, setHeaderSuccess] = useState("");
    const [bodySuccess, setBodySuccess] = useState("");
    const [notifySuccess, setNotifySuccess] = useState(false);
    const [headerError, setHeaderError] = useState("");
    const [bodyError, setBodyError] = useState("");
    const [notifyError, setNotifyError] = useState(false);

    const handleUsername = (e) => {
        setUserName(e.target.value);
    }

    const handlePassword = (e) => {
        setPassWord(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        let auth = {
            username: userName,
            password: passWord
        };

        // authenticate and set Jwt response and set Login to true..
        axios.post("/authenticate", auth)
        .then((response) => {
            setJwt(response.data);
            setLoggedIn(true);
            alertFunction(true);
        })
        .catch((error) => {
            console.log(error);
            alertFunction(false);
        });
    }

    useEffect(() => {
        if(Jwt !== "") {
            // clear first.
            localStorage.clear();
            console.log("Jwt: " + Jwt);
            console.log("Login status: " + LoggedIn);
            // save to localstorage.
            localStorage.setItem("token", Jwt);
            localStorage.setItem("status", LoggedIn);
            navigate("/authenticate");
            
        }
    });


    // alert redirection.
    const alertFunction = (notif) => {
        if (notif) {
            setHeaderSuccess("Login success!");
            setBodySuccess("You have successfully logged in!");
            setNotifySuccess(true);

            // redirect to login page.
            // navigate("/login");
        } else {
            setHeaderError("Login failed!");
            setBodyError("Check if your credentials are correct then try again.");
            setNotifyError(true);
        }
    }

    return(
        <div className="container">

            {/* notification */}
            <Success header={headerSuccess} body={bodySuccess} notify={notifySuccess}/>
            <Error header={headerError} body={bodyError} notify={notifyError}/>

            <Form className="form-style w-50" onSubmit={handleSubmit}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" placeholder="Enter username" required value={userName} onChange={handleUsername} />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required value={passWord} onChange={handlePassword} />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </div>
    )
}

export default Login;