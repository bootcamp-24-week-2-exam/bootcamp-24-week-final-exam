import React, { useEffect, useState } from "react";
import ProductContext from "../context/ProductContext"
import ProductList from "../components/Products/ProductList";
import axios from "../api/axios";
import NavbarPub from "../components/NavbarPub";
import SearchFilter from "../components/Products/SearchFilter";
import "../styles/Home-style.css";

const Home = () => {

    // get and store data using useState.
    const [productDetails, setProductDetails] = useState([]);
    const [productPage, setProductPage] = useState(0);
    const [size, setSize] = useState(10);
    const [totalpages, setTotalPages] = useState(0);
    const [filterby, setFilterBy] = useState(1);
    const [sortby, setSortBy] = useState(1);
    const [search, setSearch] = useState("");

    const [endpoint, setEndpoint] = useState(`/product/?page=${productPage}&size=${size}`);
    


    // get intial info about products default
    useEffect(() => {
        async function fetchData() {
            console.log(endpoint);
            const response = await axios.get(endpoint);
            // set product details to response.
            setProductDetails(response.data.content);
            // set total pages.
            setTotalPages(response.data.totalPages);
        }
        fetchData();
    }, [productPage, endpoint]);


    // useEffect(() => {
    //     if(productDetails.length !== 0) {
    //         // console.log(productDetails);
    //         // console.log(totalpages);
    //     }
    // }, [productDetails, setProductDetails, totalpages, setTotalPages]);

    return(
        <div className="container">
            {/* display navbar */}
            <NavbarPub imageLoc={"./images/cart-icon.png"}/>
            <ProductContext.Provider value ={{ productDetails, setProductDetails, productPage, setProductPage, size, setSize, totalpages, setTotalPages, filterby, setFilterBy, sortby, setSortBy, search, setSearch, endpoint, setEndpoint}}>
                <SearchFilter />
                <ProductList />
            </ProductContext.Provider>
        </div>
    )
}

export default Home;