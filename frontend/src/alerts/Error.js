import { useEffect, useState } from "react";
import { Alert } from "react-bootstrap";

const Error = ({header ,body, notify}) => {
    const [show, setShow] = useState(notify);

    // use effect.
    useEffect(() => {
        setShow(notify);
    }, [notify])

    if (show) {
      return (
        // <Alert variant="danger" onClose={() => setShow(false)} dismissible>
        <Alert variant="danger">
          <Alert.Heading>{ header }</Alert.Heading>
          <p>
            {body}
          </p>
        </Alert>
      );
    }
    // return <Button onClick={() => setShow(notify)}>Show Alert</Button>;
  }

  export default Error;