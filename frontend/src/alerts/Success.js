import { useEffect, useState } from "react";
import { Alert } from "react-bootstrap";

const Success = ({header ,body, notify}) => {
    const [show, setShow] = useState(false);

    useEffect(() => {
        setShow(notify);
    }, [notify])

    if (show) {
      return (
        // <Alert variant="success" onClose={() => setShow(false)} dismissible>
        <Alert variant="success">
          <Alert.Heading>{ header }</Alert.Heading>
          <p>
            {body}
          </p>
        </Alert>
      );
    }
    // return <Button onClick={() => setShow(notify)}>Show Alert</Button>;
  }

  export default Success;