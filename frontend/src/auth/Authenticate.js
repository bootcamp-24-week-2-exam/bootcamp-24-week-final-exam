import axios from "../api/axios";
import React, { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import UserContext from "../context/UserContext";
import { Spinner } from "react-bootstrap";


const Authenticate = () => {

    const { setRole } = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
      if((localStorage.getItem("token") !== null || localStorage.getItem("token")) && (localStorage.getItem("status") !== false || localStorage.getItem("status") !== "")) {

        // try to authenticate.
        const AuthStr = 'Bearer ' + localStorage.getItem("token"); 
        axios.get("/user/role", {
              headers: { Authorization: AuthStr }
        })
        .then((response) => {
          // display response.
          console.log(response.data[0].authority);
          // set user role
          setRole(response.data[0].authority);
          localStorage.setItem("role", response.data[0].authority);
          // condition to redirect base on role.
          if(response.data[0].authority === "ROLE_ADMIN") {
              navigate("/admin/profile");
          } else if(response.data[0].authority === "ROLE_CLIENT") {
              navigate("/client/profile");
          }
        })
        .catch((error) => {
          // display error.
          console.log(error);
        });

        } else {
            // redirect back to login page.
            localStorage.clear();
            navigate("/login");
        }
    }, [navigate, setRole]);

    return(
        <div className="Authenticate">
            <Spinner animation="border" variant="primary" />
        </div>
    )
}

export default Authenticate;