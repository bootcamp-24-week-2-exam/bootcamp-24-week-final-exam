import axios from "../api/axios";
import React, { useContext, useEffect } from "react";
import UserContext from "../context/UserContext";
import { useNavigate } from "react-router-dom";
import NavbarAdmin from "../components/NavbarAdmin";
import SidebarAdmin from "../components/SidebarAdmin";
import "../styles/Admin-style.css";
import NavbarPub from "../components/NavbarPub";
import { Modal, Button } from "react-bootstrap";

const AdminProfile = () => {
  // set user details and use.
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (
      (localStorage.getItem("token") !== null ||
        localStorage.getItem("token")) &&
      (localStorage.getItem("status") !== false ||
        localStorage.getItem("status") !== "") &&
      localStorage.getItem("role") !== "ROLE_CLIENT"
    ) {
      // get request for information.
      const AuthStr = "Bearer " + localStorage.getItem("token");
      axios
        .get("/admin/profile", {
          headers: { Authorization: AuthStr },
        })
        .then((response) => {
          // display response.
          console.log(response.data);
          // if(response.data === "" || response.data === null || response.data === []) {
          //     navigate("/login");
          // } else {
          //     // set user role
          //     setUser(response.data);
          // }
          setUser(response.data);
        })
        .catch((error) => {
          // display error.
          console.log(error);
        });
    } else {
      localStorage.clear();
      navigate("/login");
    }
  }, [navigate, setUser]);

  const handleLogout = () => {
    localStorage.clear();
    window.location.reload(false);
  };

  return (
    // <div className="container">
    //     <NavbarPub imageLoc={"../images/cart-icon.png"} />
    //     <SidebarAdmin />
    //         {/* access object using user.firstname and etc. */}
    //         {/* {JSON.stringify(user)} */}
    //     <button onClick={handleLogout}>logout</button>
    // </div>

    <div className="container">
      <NavbarPub imageLoc={"../images/cart-icon.png"} />
      <SidebarAdmin />

      <div className="card">
        <img src="img.jpg" alt="admin" style={{ width: "100%" }} />
        <h1>
          {user.firstname} {user.lastname}
        </h1>
        <p className="email">{user.email}</p>
        {/* <p>{user.address}</p> */}

        <p>
          <Button
            variant="primary"
            onClick={(e) => (window.location.href = "/admin/products")}
          >
            Show My Products
          </Button>
        </p>
      </div>
    </div>
  );
};

export default AdminProfile;
