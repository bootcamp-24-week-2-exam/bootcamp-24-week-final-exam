// import Head from "next/head";
// import NextLink from "next/link";
// import { useRouter } from 'next/router';
import { Modal, Form, Container, Row, Col } from "react-bootstrap";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  Box,
  Button,
  Checkbox,
  FormHelperText,
  Link,
  TextField,
  Typography,
} from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { useState } from "react";
import { useEffect } from "react";

const ProductAdd = ({
  // handleClose,
  SEND_REQUEST,
  product,
  dispatch,
  state,
}) => {
  //const router = useRouter();
  const initialImg = "";
  const [images, setImages] = useState(initialImg);
  const [title, setTitle] = useState(initialImg);
  const formik = useFormik({
    initialValues: {
      image: "",
      productName: "",
      description: "",
      quantity: "",
      price: "",
      sku: "",
      category: "",
      brand: "",
      size: "",
      color: "",
      createdAt: "",
    },
    validationSchema: Yup.object({
      // email: Yup.string()
      //   .email("Must be a valid email")
      //   .max(255)
      //   .required("Email is required"),
      productName: Yup.string().max(255).required("Product name is required"),
      description: Yup.string().max(255).required("Description is required"),
      quantity: Yup.string().max(255).required("Quantity is required"),
      price: Yup.string().max(255).required("Price is required"),
      category: Yup.string().max(255).required("Category is required"),
      brand: Yup.string().max(255).required("Brand is required"),
      size: Yup.string().max(255).required("Size is required"),
      color: Yup.string().max(255).required("Color is required"),
      sku: Yup.string().max(255).required("SKU is required"),
      //   policy: Yup.boolean().oneOf([true], "This field must be checked"),
    }),
    onSubmit: () => {
      //   router.push('/');
      console.log("onsubmit");
    },
  });
  useEffect(() => {
    if (state.action === "add") {
      setTitle("Add ");
    } else if (state.action === "edit") {
      setTitle("Update ");
    } else if (state.action === "view") {
      setTitle("");
    }
  }, [state.isOpen]);

  const handleSaveSubmit = (e) => {
    e.preventDefault();
    const toSend = { data: formik.values, image: images };
    SEND_REQUEST("POST", toSend, product.currentPage, product.productPerPage);

    formik.values.image = "";
    formik.values.productName = "";
    formik.values.description = "";
    formik.values.quantity = "";
    formik.values.price = "";
    formik.values.sku = "";
    formik.values.category = "";
    formik.values.brand = "";
    formik.values.size = "";
    formik.values.color = "";
    formik.values.createdAt = "";
    setImages(initialImg);
    dispatch({
      type: state.action,
      payload: {
        do: state.action,
        isOpenClose: false,
      },
    });
  };
  return (
    <>
      <Modal
        show={state.isOpen}
        onHide={() => {
          setImages(initialImg);
          dispatch({
            type: state.action,
            payload: {
              do: state.action,
              isOpenClose: false,
            },
          });
        }}
        animation={false}
      >
        <Form onSubmit={handleSaveSubmit}>
          <Modal.Header closeButton>
            <Modal.Title>
              <Box sx={{ my: 1 }}>
                <Typography color="textPrimary" variant="h4">
                  {`${title}Product`}
                </Typography>
                <Typography color="textSecondary" gutterBottom variant="body2">
                  `Enter your product details here`
                </Typography>
              </Box>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Box sx={{}}>
              <input
                type="file"
                name="file"
                // multiple
                accept="image/jpeg"
                onChange={(e) => {
                  // setImages([...images, ...e.target.files]);
                  setImages(e.target.files[0]);
                }}
                className="mb-1"
              />
              {images !== "" && (
                <Container>
                  <Row>
                    <Col>
                      <img
                        width="100"
                        height="100"
                        alt="image1"
                        src={
                          // images.length >= 1
                          //   ? URL.createObjectURL(images[0])
                          //   : ""
                          "File" in window && images instanceof File
                            ? URL.createObjectURL(images)
                            : ""
                        }
                      />
                    </Col>
                    {/* <Col>
                      <img
                        width="100"
                        height="100"
                        alt="image2"
                        src={
                          images.length >= 2
                            ? URL.createObjectURL(images[1])
                            : ""
                        }
                      />
                    </Col>
                    <Col>
                      <img
                        width="100"
                        height="100"
                        alt="image3"
                        src={
                          images.length >= 3
                            ? URL.createObjectURL(images[2])
                            : ""
                        }
                      />
                    </Col> */}
                    <Button
                      className="mt-2"
                      color="error"
                      onClick={() => {
                        setImages(initialImg);
                      }}
                    >
                      Remove This Image
                    </Button>
                  </Row>
                </Container>
              )}
              <Container hidden={state.action === "edit" ? false : true}>
                <Row>Select your product image/jpeg</Row>
              </Container>
            </Box>

            <Box sx={{}}>
              <TextField
                error={Boolean(
                  formik.touched.productName && formik.errors.productName
                )}
                fullWidth
                helperText={
                  formik.touched.productName && formik.errors.productName
                }
                label="Product Name"
                margin="normal"
                name="productName"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.productName}
                variant="outlined"
              />
              <TextField
                error={Boolean(
                  formik.touched.description && formik.errors.description
                )}
                fullWidth
                helperText={
                  formik.touched.description && formik.errors.description
                }
                label="Description"
                margin="normal"
                name="description"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.description}
                variant="outlined"
              />
              {/* <TextField
              error={Boolean(formik.touched.email && formik.errors.email)}
              fullWidth
              helperText={formik.touched.email && formik.errors.email}
              label="Email Address"
              margin="normal"
              name="email"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              type="email"
              value={formik.values.email}
              variant="outlined"
            /> */}
              <TextField
                error={Boolean(
                  formik.touched.quantity && formik.errors.quantity
                )}
                fullWidth
                helperText={formik.touched.quantity && formik.errors.quantity}
                label="Quantity"
                margin="normal"
                name="quantity"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.quantity}
                type="number"
                variant="outlined"
              />
              <TextField
                error={Boolean(formik.touched.price && formik.errors.price)}
                fullWidth
                helperText={formik.touched.price && formik.errors.price}
                label="Price"
                margin="normal"
                name="price"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.price}
                variant="outlined"
              />
              <TextField
                error={Boolean(
                  formik.touched.category && formik.errors.category
                )}
                fullWidth
                helperText={formik.touched.category && formik.errors.category}
                label="Category"
                margin="normal"
                name="category"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.category}
                variant="outlined"
              />
              <TextField
                error={Boolean(formik.touched.brand && formik.errors.brand)}
                fullWidth
                helperText={formik.touched.brand && formik.errors.brand}
                label="Brand"
                margin="normal"
                name="brand"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.brand}
                variant="outlined"
              />
              <TextField
                error={Boolean(formik.touched.size && formik.errors.size)}
                fullWidth
                helperText={formik.touched.size && formik.errors.size}
                label="Size"
                margin="normal"
                name="size"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.size}
                variant="outlined"
              />
              <TextField
                error={Boolean(formik.touched.color && formik.errors.color)}
                fullWidth
                helperText={formik.touched.color && formik.errors.color}
                label="Color"
                margin="normal"
                name="color"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.color}
                variant="outlined"
              />
            </Box>
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                ml: -1,
              }}
            >
              {/* <Checkbox
                checked={formik.values.policy}
                name="policy"
                onChange={formik.handleChange}
              /> */}
              {/* <Typography color="textSecondary" variant="body2">
                I have read the
                 <NextLink href="#" passHref>
                <Link color="primary" underline="always" variant="subtitle2">
                  Terms and Conditions
                </Link>
                </NextLink>
              </Typography> */}
            </Box>
          </Modal.Body>
          <Modal.Footer>
            <Box sx={{ py: 2, width: "100%" }}>
              <Button
                color="primary"
                disabled={formik.isSubmitting}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                className="mb-2"
                // onClick={handleSaveSubmit}
              >
                Save
              </Button>
              <Button
                color="error"
                disabled={formik.isSubmitting}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                className="mb-2"
                onClick={() => {
                  setImages(initialImg);
                  dispatch({
                    type: "MODAL",
                    payload: { do: "add", isOpenClose: false },
                  });
                }}
              >
                Cancel
              </Button>
            </Box>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default ProductAdd;
