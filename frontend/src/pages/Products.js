import React, { useState, useEffect, useReducer, useContext } from "react";
import ProductCsv from "./../components/ProductCsv";
import SearchProduct from "../components/SearchProduct";
import TableProduct from "../components/TableProduct";
import ProductEdit from "../components/ProductEdit";
import { Button } from "react-bootstrap";
import { INITIAL_STATE } from "../hooks/ProductHooks";
import Swal from "sweetalert2";
import axios from "../api/axios";
import { Backdrop, CircularProgress } from "@mui/material";
import ProductAdd from "./ProductAdd";
import UserContext from "../context/UserContext";
import { useNavigate } from "react-router-dom";
import productReducer from "../hooks/productReducer";
import NavbarPub from "../components/NavbarPub";
import "../styles/Product-style.css";
import Report from "./../components/Report";

const Products = () => {
  const BASE = "/product/";

  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(false);
  const [genPdf, setGenPdf] = useState(false);
  const [product, setProduct] = useState({
    products: [],
    search: "",
    currentPage: 0,
    productPerPage: 10,
    sortDir: "asc",
    isEmpty: false,
  });

  // set user details and use.
  const { setUser } = useContext(UserContext);
  const navigate = useNavigate();
  const [adminId, setAdminId] = useState(1);

  useEffect(() => {
    if (
      (localStorage.getItem("token") !== null ||
        localStorage.getItem("token")) &&
      (localStorage.getItem("status") !== false ||
        localStorage.getItem("status") !== "") &&
      localStorage.getItem("role") !== "ROLE_CLIENT"
    ) {
      // get request for information.
      const AuthStr = "Bearer " + localStorage.getItem("token");
      axios
        .get("/admin/profile", {
          headers: { Authorization: AuthStr },
        })
        .then((response) => {
          // display response.
          console.log(response.data);

          setAdminId(response.data.id);
          setUser(response.data);
        })
        .catch((error) => {
          // display error.
          console.log(error);
        });
    } else {
      localStorage.clear();
      navigate("/login");
    }
  }, [navigate, setUser]);

  const [state, dispatch] = useReducer(productReducer, INITIAL_STATE);

  const SEND_REQUEST = async (
    action = "GET",
    endPoint = "",
    page = 0,
    size = 10
  ) => {
    try {
      //e.preventDefault();
      setLoading(true);
      let response = "";
      switch (action) {
        case "POST":
          const d = new Date().getTime();
          const dataP = { ...endPoint.data, admin_id: adminId, image: d };

          const productImg = new FormData();
          productImg.append("files", endPoint.image);

          productImg.append("productName", dataP.productName);
          productImg.append("category", dataP.category);
          productImg.append("brand", dataP.brand);
          productImg.append("size", dataP.size);
          productImg.append("color", dataP.color);
          productImg.append("time", dataP.image);

          response = await axios
            .post(`${BASE}img`, productImg, dataP)
            .then((res) => {
              const response2 = axios
                .post(`${BASE}`, dataP)
                .then((res2) => {
                  Swal.fire(
                    "ADDED",
                    `The ${res2?.data.message} was successfully added.`,
                    "success"
                  );
                  SEND_REQUEST(
                    "GET",
                    search,
                    product.currentPage,
                    product.productPerPage
                  );
                })
                .catch((err) => {
                  console.log(err);
                  Swal.fire("ERROR", "Something wrong...", "error");
                });
            })
            .catch((err) => {
              console.log(err);
              Swal.fire("ERROR", "Something wrong...", "error");
            });

          break;
        case "PUT":
          const dd = new Date().getTime();

          const dataPP =
            "File" in window && endPoint.image instanceof File
              ? { ...endPoint.data, admin_id: adminId, image: dd }
              : { ...endPoint.data, admin_id: adminId };

          const productImgg = new FormData();
          productImgg.append("files", endPoint.image);

          productImgg.append("productName", dataPP.productName);
          productImgg.append("category", dataPP.category);
          productImgg.append("brand", dataPP.brand);
          productImgg.append("size", dataPP.size);
          productImgg.append("color", dataPP.color);
          productImgg.append("time", dataPP.image);

          (await "File") in window && endPoint.image instanceof File
            ? (response = axios
                .post(`${BASE}img`, productImgg, dataPP)

                .then((res) => {
                  console.log("uploaded");
                })
                .catch((err) => {
                  console.log(err);
                  Swal.fire("ERROR", "Something wrong...", "error");
                }))
            : console.log("No new img");

          const response2 = axios
            .put(`${BASE}`, dataPP)
            .then((res2) => {
              Swal.fire(
                "UPDATED",
                `The ${res2?.data.message} was successfully updated.`,
                "success"
              );
              SEND_REQUEST(
                "GET",
                search,
                product.currentPage,
                product.productPerPage
              );
            })
            .catch((err) => {
              console.log(err);
              Swal.fire("ERROR", "Something wrong...", "error");
            });
          break;
        case "GET":
          response = await axios
            .get(`${BASE}${endPoint}?page=${page}&size=${size}`)
            .then((res) => {
              setProduct({
                ...product,
                products: res?.data?.content,
                totalPages: res?.data?.totalPages,
                totalElements: res?.data?.totalElements,
                currentPage: res?.data?.number,
                productPerPage: 10,
                // productPerPage:
                //   res?.data?.numberOfElements === 0
                //     ? 10
                //     : res?.data?.numberOfElements,
                isEmpty: res?.data?.empty,
              });
            })
            .catch((err) => {
              console.log("ErrorRes", err);
              dispatch({ type: "ERROR" });
            });
          break;
        case "DELETE":
          response = await axios
            .delete(`${BASE}${endPoint}`)
            .then((res) => {
              SEND_REQUEST(
                "GET",
                search,
                product.currentPage,
                product.productPerPage
              );
              Swal.fire(
                "DELETED",
                `The ${res?.data.message} was successfully deleted.`,
                "success"
              );
            })
            .catch((err) => {
              console.log(err);
              Swal.fire("ERROR", "Something wrong...", "error");
            });

          break;
        case "TRANSACTION":
          const AuthStr = "Bearer " + localStorage.getItem("token");
          response = await axios
            .get("/transaction/date/pdf?min=2022-09-15&max=2022-09-16", {
              headers: { Authorization: AuthStr },
            })
            .then((res) => {
              console.log("pdf:", res.data);
              const filename = res.headers
                .get("Content-Disposition")
                .split("filename=")[1];
              res.blob().then((blob) => {
                let url = window.URL.createObjectURL(blob);
                let a = document.createElement("a");
                a.href = url;
                a.download = filename;
                a.click();
              });
            })
            .catch((err) => {
              console.log(err);
              Swal.fire("ERROR", "Something wrong...", "error");
            });

          break;
        default:
          break;
      }
      setLoading(false);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    SEND_REQUEST("GET", search, product.currentPage, product.productPerPage);
  }, []);

  return (
    <div className="container">
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={loading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      {/* <NavbarAdmin /> */}
      <NavbarPub imageLoc={"../images/cart-icon.png"} />
      <div className="container mt-2 mb-3">
        <div className="row">
          <div className="card p-4 mt-3">
            <section className="section-pagetop bg mb-3 mt-1">
              <div className="container">
                <h2 className="title-page">Products</h2>
              </div>
            </section>
            <nav>
              <div className="nav nav-tabs" id="nav-tab" role="tablist">
                <button
                  className="nav-link active"
                  id="nav-home-tab"
                  data-bs-toggle="tab"
                  data-bs-target="#nav-home"
                  type="button"
                  role="tab"
                  aria-controls="nav-home"
                  aria-selected="true"
                >
                  Products
                </button>
                {/* <button
                  className="nav-link"
                  id="nav-history-tab"
                  data-bs-toggle="tab"
                  data-bs-target="#nav-history"
                  type="button"
                  role="tab"
                  aria-controls="nav-history"
                  aria-selected="false"
                >
                  Logs History
                </button> */}
              </div>
            </nav>
            <div className="tab-content" id="nav-tabContent">
              <div
                className="tab-pane fade show active"
                id="nav-home"
                role="tabpanel"
                aria-labelledby="nav-home-tab"
                tabIndex="0"
              >
                <div className="col-md-12">
                  {/* <p className="badge bg-dark mt-4">
                    Total number of products: {product.totalElements}
                  </p> */}
                  <Button
                    className="btn btn-primary m-2 float-start"
                    size="lg"
                    href="http://localhost:8080/transaction/date/pdf?start=2022-09-15&end=2022-09-16"
                    variant="primary"
                    // onClick={(e) => setGenPdf(true)}
                  >
                    Download PDF File (Transactions)
                  </Button>

                  <Button
                    variant="primary"
                    size="lg"
                    className="btn btn-primary m-2 float-end"
                    // onClick={handleOpen}
                    onClick={() =>
                      dispatch({
                        type: "add",
                        payload: { do: "add", isOpenClose: true },
                      })
                    }
                  >
                    Add Product
                  </Button>

                  <ProductCsv
                    product={product}
                    SEND_REQUEST={SEND_REQUEST}
                    search={search}
                    adminId={adminId}
                  />

                  <SearchProduct
                    product={product}
                    SEND_REQUEST={SEND_REQUEST}
                    setSearch={setSearch}
                    search={search}
                  />

                  <TableProduct
                    product={product}
                    SEND_REQUEST={SEND_REQUEST}
                    search={search}
                    dispatch={dispatch}
                  />
                  {state.action === "add" && (
                    <ProductAdd
                      SEND_REQUEST={SEND_REQUEST}
                      product={product}
                      dispatch={dispatch}
                      state={state}
                    />
                  )}

                  {state.action === "edit" && (
                    <ProductEdit
                      SEND_REQUEST={SEND_REQUEST}
                      product={product}
                      dispatch={dispatch}
                      state={state}
                    />
                  )}

                  {state.action === "view" && (
                    <ProductEdit
                      SEND_REQUEST={SEND_REQUEST}
                      product={product}
                      dispatch={dispatch}
                      state={state}
                    />
                  )}

                  {genPdf && <Report />}
                </div>
              </div>
              <div
                className="tab-pane fade"
                id="nav-history"
                role="tabpanel"
                aria-labelledby="nav-history-tab"
                tabIndex="0"
              >
                <section className="section-pagetop bg mb-3 mt-3">
                  <div className="container">
                    <div className="row align-items-center">
                      <div className="col">
                        <p className="title-page h4">Tab2</p>
                      </div>

                      <div className="col">
                        <button
                          className="btn btn-success m-2 float-end"
                          //   onClick={handlePrint}
                        >
                          {/* <FontAwesomeIcon icon={faPrint} />  */}
                          Print
                        </button>
                      </div>
                    </div>
                  </div>
                </section>
                <div style={{ display: "none" }}>
                  {/* <ComponentToPrint logs={logs} 
                  ref={componentRef} /> */}
                </div>
                <table className="table table-striped table-hover text-center">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Role Name</th>
                      <th>Description</th>
                      <th>Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    {/* dito */}
                    {/* {logs.map((l) => {
                      return (
                        <tr>
                          <td>{l.id}</td>
                          <td>{l.role}</td>
                          <td>{l.action}</td>
                          <td>June 02, 2022</td>
                        </tr>
                      );
                    })} */}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Products;
