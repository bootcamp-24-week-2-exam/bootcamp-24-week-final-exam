import React from 'react';
import { Box, Button, TextField, Typography } from "@mui/material";
import { Modal, Form, Container, Row, Col } from "react-bootstrap";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import  { useState } from "react";
import Swal from 'sweetalert2';
import axios from '../api/axios';




function Card() {

    const [isSelect, setSelect] = useState(false);
    const handleChange = (e) => {
        if (e.target.value === 1){
            setSelect (true)
        }
        else
        setSelect (false)
      };

      const [isClose, setClose] = useState(true);
      const handleClose = (e) => { 
        e.preventDefault();
            Swal.fire({
              title: "Do you want to CANCEL this card?",
              showDenyButton: true,
              confirmButtonText: "Yes",
              denyButtonText: `No`,
              icon: "warning",
            }).then((result) => {
              if (result.isConfirmed) {
                setClose (false) 
              }
            });   

      };


    const [cardName, setcardName] = useState("");
    const [cardNumber, setcardNumber] = useState("");
    const [cardType, setcardType] = useState("");
    const [cardCvc, setcardCvc] = useState("");
    const [cardExpiry, setcardExpiry] = useState("");
    const [cardBalance, setcardBalance] = useState("");
    const [userId, setuserId] = useState("");


    // functions for onchange.
    const handlecardName = (e) => {
        setcardName(e.target.value);
        // console.log(e.target.value);
    }
    const handlecardNumber = (e) => {
        setcardNumber(e.target.value);
        // console.log(e.target.value);
    }
    const handlecardType = (e) => {
        setcardType(e.target.value);
        // console.log(e.target.value);
    }
    const handlecardCvc = (e) => {
        setcardCvc(e.target.value);
        // console.log(e.target.value);
    }
    const handlecardExpiry= (e) => {
        setcardExpiry(e.target.value);
        // console.log(e.target.value);
    }
    const handlecardBalance = (e) => {
        setcardBalance(e.target.value);
        // console.log(e.target.value);
    }
    const handleuserId = (e) => {
        setuserId(e.target.value);
        // console.log(e.target.value);
    }
    
    


      const save = async (e) => {

        let addnewcard = 
        {
            name: cardName,
            number: cardNumber,
            type: cardType,
            cvc: cardCvc,
            expiry: cardExpiry,
            balance: cardBalance,
            user_id: userId,
        };

        e.preventDefault();
        try {
            const AuthStr = 'Bearer ' + localStorage.getItem("token"); 
            const response = await axios
          
            .post("/card/addcard",addnewcard, {headers: { Authorization: AuthStr }})
            
            .then(function (response) {
              Swal.fire(
                "Card Information Saved!",
                "Successfully Added to the mode of payments!",
                "success"
              );

            })
        } catch (e) {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Cannot save card information!",
          });
        }
      };



  return (
<>
    <Modal  show={isClose} animation={false}>
        <Form>
          <Modal.Header>
            <Modal.Title>
              <Box sx={{ my: 1 }}>
                <Typography color="textPrimary" variant="h4">
                  {`Add Card`}
                </Typography>
                <Typography color="textSecondary" gutterBottom variant="body2">
                  Enter your card details here:
                </Typography>
              </Box>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>

          <FormControl variant="standard" sx={{ m: 1, minWidth: "100%" }}>
        <InputLabel id="demo-simple-select-standard-label">Card Type</InputLabel>
        <Select
          labelId="demo-simple-select-standard-label"
          id="demo-simple-select-standard"
          label="Age"
          onChange= { (e) => {handleChange(e); handlecardType();} }
        >
          <MenuItem value={1}>Credit</MenuItem>
          <MenuItem value={2}>Debit</MenuItem>
        </Select>
      </FormControl>

          <TextField
                fullWidth
                label="Name"
                margin="normal"
                name="cardName"
                variant="outlined"
                required
                onChange={handlecardName}
              />
              <TextField
                fullWidth
                label="Number"
                margin="normal"
                name="cardNumber"
                variant="outlined"
                required
                onChange={handlecardNumber}
              />
              
              <TextField
                fullWidth
                label="Cvc"
                margin="normal"
                name="cardCvc"
                variant="outlined"
                required
                onChange={handlecardCvc}
              />

              <TextField
                fullWidth
                label="Expiry"
                margin="normal"
                name="cardExpiry"
                variant="outlined"
                required
                onChange={handlecardExpiry}
              />

              <TextField
                fullWidth
                label="Balance"
                margin="normal"
                name="cardBalance"
                variant="outlined"
                required
                onChange={handlecardBalance}
                disabled={isSelect}
              />

              <TextField
                fullWidth
                label="User ID"
                margin="normal"
                name="userId"
                variant="outlined"
                required
                onChange={handleuserId}
              />
          </Modal.Body>
          <Modal.Footer>
           
          <Button
                color="primary"
                disabled={false}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                className="mb-2"
                onClick={save}
              >
                Save
              </Button>

              <Button
                color="error"
                disabled={false}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                className="mb-2"
                onClick={handleClose}
              >
                Cancel
              </Button>


          </Modal.Footer>
        </Form>
      </Modal>
    </>
   
  )
}

export default Card