import React, { useContext, useEffect, useState } from "react";
import { Button, Form, Table } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import { useNavigate } from "react-router-dom";
import NavbarPub from "../components/NavbarPub";
import UserContext from "../context/UserContext";
import axios from "../api/axios";
import "../styles/Cart-style.css";
import Swal from "sweetalert2";
import PaymentMethod from "../components/PaymentMethod";

const Cart = () => {
  const IMG_PATH = "http://localhost:8080/img/";
  const BASE = "/cart/";
  const navigate = useNavigate();
  // const { user, setUser } = useContext(UserContext);
  const [user, setUser] = useState([]);

  // useState for search.
  const [search, setSearch] = useState("");
  const [filterby, setFilterBy] = useState(1);
  const [sortby, setSortBy] = useState(1);

  const [quantity, setQuantity] = useState(0);
  const [subtotal, setSubtotal] = useState(0);

  const [paymentMethod, setPaymentMethod] = useState(false);
  const [cartToCheckOut, setCartToCheckOut] = useState([]);

  const [updateData, setData] = useState(true);

  const handleAddBtn = () => {
    let sum = quantity + 1;
    setQuantity(sum);
  };

  const handleMinusBtn = () => {
    if (quantity > 1) {
      let diff = quantity - 1;
      setQuantity(diff);
    }
  };

  const handleSelect = (cart_id, qty, e) => {
    const response = axios
      .get(`${BASE}total/${cart_id}/${qty}`)
      .then((res) => {
        setSubtotal(
          e.target.checked ? res.data + subtotal : subtotal - res.data
        );
        setCartToCheckOut([...cartToCheckOut, cart_id]);
      })
      .catch((err) => {
        console.log(err);
        Swal.fire("ERROR", "Something wrong...", "error");
      });
  };

  const handleCheckout = () => {
    console.log(subtotal);
    if (subtotal > 0) {
      setPaymentMethod(true);
    }
  };

  const SEND_REQUEST = async (action = "PUT", option, cart_id) => {
    try {
      let response = "";
      switch (action) {
        case "POST":
          console.log("POSTTrans:", option);
          console.log("POSTTrans:", { ...cart_id });
          response = await axios
            .put(`${BASE}trans/${option}`, cart_id)
            .then((res) => {
              Swal.fire("Pay", `The payment was successfully save.`, "success");

              // SEND_REQUEST(
              //   "GET",
              //   search,
              //   product.currentPage,
              //   product.productPerPage
              // );
            })
            .catch((err) => {
              console.log(err);
              Swal.fire("ERROR", "Something wrong...", "error");
            });

          break;
        case "PUT":
          response = axios
            .put(`${BASE}${option}/${cart_id}`)
            .then((res) => {
              setQuantity(res.data);
            })
            .catch((err) => {
              console.log(err);
              Swal.fire("ERROR", "Something wrong...", "error");
            });
          break;
        case "GET":
          response = axios
            .get(`${BASE}total/${cart_id}/${option}`)
            .then((res) => {
              setSubtotal(res.data + subtotal);
            })
            .catch((err) => {
              console.log(err);
              Swal.fire("ERROR", "Something wrong...", "error");
            });
          break;
        case "DELETE":
          // response = await axios
          //   .delete(`${BASE}${endPoint}`)
          //   .then((res) => {
          //     SEND_REQUEST(
          //       "GET",
          //       search,
          //       product.currentPage,
          //       product.productPerPage
          //     );
          //     Swal.fire(
          //       "DELETED",
          //       `The ${res?.data.message} was successfully deleted.`,
          //       "success"
          //     );
          //   })
          //   .catch((err) => {
          //     console.log(err);
          //     Swal.fire("ERROR", "Something wrong...", "error");
          //   });

          break;
        default:
          break;
      }
      setData(true);
    } catch (e) {
      console.log(e);
    }
  };

  const handleSearch = (e) => {
    setSearch(e.target.value);
    // console.log(search);
  };

  const handleFilterBy = (e) => {
    setFilterBy(e.target.value);
    // console.log(filterby);
  };

  const handleSortBy = (e) => {
    setSortBy(e.target.value);
    console.log(sortby);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // // use axios to search.
    // axios.get({})
  };

  // handle remove.
  const handleRemove = () => {
    // query to remove in cart table  databaase;
    localStorage.removeItem("product");
    localStorage.removeItem("quantity");
  };

  const handlePage = (page) => {
    // axios.get(`/product/?page=${page.selected}&size=${size}`)
    // .then((response) => {
    //     console.log(response.data.content);
    //     // set productDetails to response.data.content.
    //     setProductDetails([response.data.content]);
    //     // set productPage to page.
    //     setProductPage(page.selected);
    // })
    // .catch((error) => {
    //     console.log(error);
    // });
    console.log(page.selected);
  };

  useEffect(() => {
    // get request for information.
    const AuthStr = "Bearer " + localStorage.getItem("token");
<<<<<<< HEAD
=======

    // cartlist length
>>>>>>> d401ed5d64fc03d079d1b0a960e27d6f4b8cbc2c
    axios
      .get("/client/profile", {
        headers: { Authorization: AuthStr },
      })
      .then((response) => {
        // display response.
        console.log(response.data);
        setUser(response.data);
      })
      .catch((error) => {
        // display error.
        console.log(error);
      });
<<<<<<< HEAD
    setData(false);
  }, [updateData]);
=======
<<<<<<< HEAD
  }, [user]);

  // useEffect(() => {
  //   console.log(user.cartList);
  //   setQuantity(user.cartList.length);
  // }, []);
=======
  }, []);
>>>>>>> 9aba0a2aa35a0c9d3e2202a2146dab44e85ecb68
>>>>>>> d401ed5d64fc03d079d1b0a960e27d6f4b8cbc2c

  useEffect(() => {
    if (
      (localStorage.getItem("token") !== null ||
        localStorage.getItem("token")) &&
      (localStorage.getItem("status") !== false ||
        localStorage.getItem("status") !== "") &&
      localStorage.getItem("role") !== "ROLE_ADMIN"
    ) {
      navigate("/client/cart");
    } else {
      navigate("/login");
    }
  }, [navigate]);

  if (
    localStorage.getItem("role") === "ROLE_CLIENT"
    // &&
    // user.cartList.product !== null
  ) {
    return (
      <div className="container">
        {/* navigation */}
        <NavbarPub imageLoc={"../images/cart-icon.png"} />
        <div className="container bg-white rounded-top mt-5" id="zero-pad">
          <Form className="d-flex search-field" onSubmit={handleSubmit}>
            <Form.Select
              className="filter-field"
              aria-label="Default select example"
              value={filterby}
              onChange={handleFilterBy}
            >
              <option>Filter by</option>
              <option value="1">Product name</option>
              <option value="2">Category(top/bottom)</option>
              <option value="3">Product sizes</option>
              <option value="4">Product color</option>
            </Form.Select>
            <Form.Select
              className="sort-field"
              aria-label="Default select example"
              value={sortby}
              onChange={handleSortBy}
            >
              <option>Sort by</option>
              <option value="1">Ascending</option>
              <option value="2">Descending</option>
            </Form.Select>
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
              value={search}
              onChange={handleSearch}
              required
            />
            <Button variant="outline-success" type="submit">
              Search
            </Button>
          </Form>

          {user.cartList && (
            <div className="row d-flex justify-content-center">
              <div className="col-lg-10 col-12 pt-3">
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>Select</th>
                      <th>Product Image</th>
                      <th>Product</th>
                      <th>Description</th>
                      <th>Price</th>
                      <th>Quantity</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tbody>
                    {user.cartList
                      .filter(({ status }) => status === 1)
                      .map((cart) => {
                        return (
                          <tr key={cart.cart_id}>
                            <td>
                              <div className="mt-5">
                                <input
                                  type="checkbox"
                                  onClick={(e) =>
                                    handleSelect(cart.cart_id, cart.quantity, e)
                                  }
                                />
                              </div>
                            </td>
                            <td>
                              <img
                                className="productimage-style"
                                alt="error"
                                src={
                                  // {`${IMG_PATH}${cart.product.image}`}
                                  cart.product.image != null
                                    ? `${IMG_PATH}${cart.product.image}`
                                    : "../images/shopping.jpg"
                                }
                              />
                            </td>
                            <td>
                              <div className="mt-5">
                                {cart.product.productName}
                              </div>
                            </td>
                            <td>
                              <div className="mt-5">
                                {cart.product.description}
                              </div>
                            </td>
                            <td>
                              <div className="mt-5">{cart.product.price}</div>
                            </td>
                            <td>
                              <div className="">
                                <Button
                                  className="wl-2 wr-2"
                                  onClick={() =>
                                    SEND_REQUEST("PUT", "add", cart.cart_id)
                                  }
                                >
                                  +
                                </Button>
                                <p className="mt-3">
                                  {quantity > 0
                                    ? ""
                                    : setQuantity(cart.quantity)}
                                  {quantity > 0 ? quantity : cart.quantity}
                                </p>
                                <Button
                                  className="pr-2 pl-2"
                                  onClick={() =>
                                    quantity === 1
                                      ? 1
                                      : SEND_REQUEST("PUT", "sub", cart.cart_id)
                                  }
                                >
                                  -
                                </Button>
                              </div>
                            </td>
                            <td>
                              <Button className="btn-danger mt-5">
                                remove
                              </Button>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
              </div>
            </div>
          )}
          <div className="container bg-light rounded-bottom py-4" id="zero-pad">
            <div className="row d-flex justify-content-center">
              <div className="col-lg-10 col-12">
                <div className="d-flex justify-content-between align-items-center">
                  <div className="px-md-0 px-1" id="footer-font">
                    <b className="pl-md-4">
                      SUBTOTAL: <span className="pl-md-4">₱ {subtotal}</span>
                    </b>
                  </div>
                  <div>
                    <button
                      className="btn btn-sm btn-primary px-lg-5 px-3"
                      onClick={handleCheckout}
                    >
                      CHECKOUT
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <ReactPaginate
            previousLabel={"<<Prev"}
            nextLabel={"Next>>"}
            breakLabel={"..."}
            pageCount={8}
            marginPagesDisplayed={3}
            pageRangeDisplayed={2}
            onPageChange={handlePage}
            containerClassName={"pagination justify-content-center"}
            pageClassName={"page-item"}
            pageLinkClassName={"page-link"}
            previousClassName={"page-item"}
            previousLinkClassName={"page-link"}
            nextClassName={"page-item"}
            nextLinkClassName={"page-link"}
            breakClassName={"page-item"}
            breakLinkClassName={"page-link"}
            activeClassName={"active"}
          />
        </div>
        {paymentMethod && (
          <PaymentMethod
            subtotal={subtotal}
            user_id={user.id}
            cardList={user.cardList}
            cartToCheckOut={cartToCheckOut}
            SEND_REQUEST={SEND_REQUEST}
          />
        )}
      </div>
    );
  } else if (
    localStorage.getItem("role") === "ROLE_CLIENT" &&
    localStorage.getItem("product") === null
  ) {
    return (
      <div className="container">
        {/* navigation */}
        <NavbarPub imageLoc={"../images/cart-icon.png"} />
        <div className="container bg-white rounded-top mt-5" id="zero-pad">
          <Form className="d-flex search-field" onSubmit={handleSubmit}>
            <Form.Select
              className="filter-field"
              aria-label="Default select example"
              value={filterby}
              onChange={handleFilterBy}
            >
              <option>Filter by</option>
              <option value="1">Product name</option>
              <option value="2">Category(top/bottom)</option>
              <option value="3">Product sizes</option>
              <option value="4">Product color</option>
            </Form.Select>
            <Form.Select
              className="sort-field"
              aria-label="Default select example"
              value={sortby}
              onChange={handleSortBy}
            >
              <option>Sort by</option>
              <option value="1">Ascending</option>
              <option value="2">Descending</option>
            </Form.Select>
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
              value={search}
              onChange={handleSearch}
              required
            />
            <Button variant="outline-success" type="submit">
              Search
            </Button>
          </Form>
          <div className="px-lg-5 mr-lg-7" id="product">
            <h3 className="mt-5 mb-5 pt-5 pb-5 border">Cart is Empty!</h3>
          </div>
        </div>
      </div>
    );
  }
};

export default Cart;
