import axios from "../api/axios";
import React, { useContext, useEffect } from "react";
import UserContext from "../context/UserContext";
import { useNavigate } from "react-router-dom";
import "../styles/ClientProfile-style.css";
import NavbarPub from "../components/NavbarPub";

const ClientProfile = () => {

    // set user details and use.
    const { user, setUser } = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        if((localStorage.getItem("token") !== null || localStorage.getItem("token")) && (localStorage.getItem("status") !== false || localStorage.getItem("status") !== "") && (localStorage.getItem("role") !== "ROLE_ADMIN")){
            
            // get request for information.
                const AuthStr = 'Bearer ' + localStorage.getItem("token"); 
                axios.get("/client/profile", {
                    headers: { Authorization: AuthStr }
                })
                .then((response) => {
                // display response.
                console.log(response.data);
                // if(response.data === "" || response.data === null || response.data === []) {
                //     navigate("/login");
                // } else {
                //     // set user role
                //     setUser(response.data);
                // }
                setUser(response.data);
                
                })
                .catch((error) => {
                // display error.
                console.log(error);
                });

        } else {
            localStorage.clear();
            navigate("/login");
        }
    }, [navigate, setUser]);

    useEffect(() => {
        localStorage.setItem("userid", user.id);
    }, [user])

    return(
        <div className="container">
            <NavbarPub imageLoc={"../images/cart-icon.png"}/>
            <div className="sideBar">
                <ul>
                    <li>{user.firstname}</li>
                </ul>
            </div>
        </div>
    )
}

export default ClientProfile;