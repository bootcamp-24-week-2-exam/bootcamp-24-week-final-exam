const PageNotFound = () => {
    return(
        <div className="PageNotFound">
            <h1>404 PAGE NOT FOUND!</h1>
        </div>
    );
}

export default PageNotFound