import React, { useState } from "react";
import Swal from "sweetalert2";
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  Pagination,
  TableRow,
  Button,
  Stack,
  Alert,
  Avatar,
} from "@mui/material";

import { Delete, Edit, Info } from "@mui/icons-material";

const TableProduct = ({ product, SEND_REQUEST, search, dispatch }) => {
  const columns = [
    { id: "productID", label: "ID", minWidth: 50 },
    { id: "image", label: "", minWidth: 150 },
    { id: "productName", label: "Product Name", minWidth: 170 },
    // { id: "description", label: "Description", minWidth: 170 },
    {
      id: "brand",
      label: "Brand",
      minWidth: 50,
      format: (value) => value.toLocaleString("en-US"),
    },
    {
      id: "price",
      label: "Price",
      minWidth: 170,
      align: "right",
      format: (value) => value,
    },
    {
      id: "quantity",
      label: "Stocks",
      minWidth: 50,
      align: "center",
    },
    // {
    //   id: "size",
    //   label: "Size",
    //   minWidth: 100,
    //   format: (value) => value.toLocaleString("en-US"),
    // },
    // {
    //   id: "color",
    //   label: "Color",
    //   minWidth: 100,
    //   format: (value) => value.toLocaleString("en-US"),
    // },
    {
      id: "action",
      label: "Action",
      minWidth: 170,
      align: "center",
      format: (value) => value.toLocaleString("en-US"),
    },
  ];
  const handleChangePage = (event, newPage) => {
    console.log("newPage:", newPage - 1);
    SEND_REQUEST("GET", search, newPage - 1, product.productPerPage);
  };

  const actionButton = (col_id, id_img, row) => {
    if (col_id === "image") {
      if (id_img === null || id_img === "") {
        id_img =
          "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png";
      } else {
        id_img = `http://localhost:8080/img/${id_img.split(",")[0]}`;
      }
      return (
        <Avatar sx={{ width: "100%", height: "100%" }} variant="rounded">
          <img src={id_img} alt="image" width="100" height="100"></img>
        </Avatar>
      );
    } else if (col_id === "action") {
      return (
        <Stack direction="column" spacing={1}>
          <Button
            variant="contained"
            startIcon={<Info />}
            onClick={() =>
              dispatch({
                type: "view",
                payload: { do: "view", isOpenClose: true, productData: row },
              })
            }
          >
            View
          </Button>
          <Button
            variant="contained"
            startIcon={<Edit />}
            onClick={() =>
              dispatch({
                type: "edit",
                payload: { do: "edit", isOpenClose: true, productData: row },
              })
            }
          >
            Edit
          </Button>
          <Button
            variant="contained"
            color="error"
            startIcon={<Delete />}
            onClick={() => {
              Swal.fire({
                title: "Do you want to DELETE this product?",
                showDenyButton: true,
                confirmButtonText: "DELETE",
                denyButtonText: `Cancel`,
                icon: "info",
              }).then((result) => {
                if (result.isConfirmed) {
                  SEND_REQUEST(
                    "DELETE",
                    id_img,
                    product.currentPage,
                    product.productPerPage
                  );
                }
              });
            }}
          >
            Delete
          </Button>
        </Stack>
      );
    }
  };

  return (
    <Paper sx={{ width: "100%" }}>
      <TableContainer sx={{ maxHeight: "100%" }}>
        <Alert
          hidden={false}
          severity={product.isEmpty ? "warning" : "success"}
        >
          Search result{" "}
          <b>
            {product.totalElements > 0 ||
            !typeof product.totalElements === "undefined"
              ? product.totalElements
              : product.products.length > 0
              ? product.products.length
              : "not found"}
          </b>
        </Alert>
        <Table
          stickyHeader
          aria-label="sticky table"
          sx={{ tableLayout: "auto" }}
        >
          <TableHead color="success">
            {/* <TableRow>
              <TableCell align="center" colSpan={2}>
                Country
              </TableCell>
              <TableCell align="center" colSpan={3}>
                Details
              </TableCell>
            </TableRow> */}
            <TableRow
              sx={{
                backgroundColor: "yellow",
                borderBottom: "2px solid black",
                "& th": {
                  fontSize: "1.25rem",
                  color: "black",
                },
              }}
            >
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{
                    top: 0,
                    minWidth: column.minWidth,
                    backgroundColor: "rgb(0, 102, 255)",
                    color: "white",
                  }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {product.products.map((row, key) => {
              return (
                <TableRow hover tabIndex={-1} key={key}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell
                        style={{
                          fontSize: "1rem",
                        }}
                        key={column.id}
                        align={column.align}
                      >
                        {column.id !== "action" && column.id !== "image"
                          ? column.format && typeof value === "number"
                            ? column.format(value)
                            : value
                          : actionButton(
                              column.id,
                              column.id === "action"
                                ? row["productID"]
                                : row["image"],
                              row
                            )}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <Stack
        direction="row"
        justifyContent="center"
        alignItems="center"
        spacing={2}
      >
        <Pagination
          shape="rounded"
          color="primary"
          count={product.totalPages}
          page={product.currentPage + 1}
          onChange={handleChangePage}
        />
      </Stack>
    </Paper>
  );
};
export default TableProduct;
