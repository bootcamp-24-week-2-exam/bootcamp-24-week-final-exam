import React, { useState } from "react";
import "../styles/SearchProduct.css";
import {
  Button,
  Container,
  Form,
  Nav,
  Navbar,
  NavDropdown,
  Offcanvas,
} from "react-bootstrap";

const SearchProduct = ({ product, SEND_REQUEST, setSearch, search }) => {
  return (
    <div className="divbg">
      {["xl"].map((expand) => (
        <Navbar key={expand} expand={expand} className="mb-3">
          <Container fluid>
            {/* <Navbar.Brand className="font-text">Search result for</Navbar.Brand> */}

            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-${expand}`}
              aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
              placement="end"
            >
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                  Search
                </Offcanvas.Title>
              </Offcanvas.Header>
              <Offcanvas.Body>
                <Nav
                  variant="pills"
                  className="justify-content-end flex-grow-1 pe-3"
                >
                  <Nav.Link href="#action2" className="font-text">
                    Sort by
                  </Nav.Link>
                  <NavDropdown
                    className="font-text"
                    title="Price "
                    id={`offcanvasNavbarDropdown-expand-${expand}`}
                  >
                    <NavDropdown.Item href="#action3">
                      Price: Low to High
                    </NavDropdown.Item>
                    <NavDropdown.Item href="#action4">
                      Price: High to Low
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action5">
                      Something else here
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav>
                <Form className="d-flex search-text">
                  <Form.Control
                    type="search"
                    placeholder="Search in One Stop Shop"
                    className="me-2"
                    size="lg"
                    aria-label="Search"
                    onChange={(e) => {
                      setSearch(e.target.value);
                    }}
                  />
                  <Button
                    size="lg"
                    variant="outline-primary"
                    onClick={() =>
                      SEND_REQUEST("GET", search, 0, product.productPerPage)
                    }
                  >
                    Search
                  </Button>
                </Form>
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
      ))}
    </div>
  );
};
export default SearchProduct;
