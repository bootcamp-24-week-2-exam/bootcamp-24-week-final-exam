import React from "react";
import { Box, Button, TextField, Typography } from "@mui/material";
import { Modal, Form, Container, Row, Col } from "react-bootstrap";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { useState } from "react";
import Swal from "sweetalert2";
import axios from "../api/axios";

function PaymentMethod({
  subtotal,
  user_id,
  cardList,
  cartToCheckOut,
  SEND_REQUEST,
}) {
  const BASE = "/transaction/";
  const [isSelect, setSelect] = useState(false);
  const handleChange = (e) => {
    // if (e.target.value === 1) {
    //   setSelect(true);
    // } else setSelect(false);
  };

  const [isClose, setClose] = useState(true);
  const handleClose = (e) => {
    e.preventDefault();
    setClose(false);
    // Swal.fire({
    //   title: "Do you want to CANCEL this card?",
    //   showDenyButton: true,
    //   confirmButtonText: "Yes",
    //   denyButtonText: `No`,
    //   icon: "warning",
    // }).then((result) => {
    //   if (result.isConfirmed) {
    //     setClose(false);
    //   }
    // });
  };

  const [myCard, setMyCard] = useState([]);
  const [card_id, setCardId] = useState(0);
  // const [cardNumber, setcardNumber] = useState("");
  // const [cardType, setcardType] = useState("");
  // const [cardCvc, setcardCvc] = useState("");
  // const [cardExpiry, setcardExpiry] = useState("");
  // const [cardBalance, setcardBalance] = useState("");
  // const [userId, setuserId] = useState("");

  // // functions for onchange.
  // const handlecardName = (e) => {
  //     setcardName(e.target.value);
  //     // console.log(e.target.value);
  // }
  // const handlecardNumber = (e) => {
  //     setcardNumber(e.target.value);
  //     // console.log(e.target.value);
  // }
  const handlecardType = (e) => {
    //setcardType(e.target.value);
    console.log("handelCradType:", e.target.value);
    setSelect(true);
    cardList.map((card) => {
      // <MenuItem value={card.userid}>{card.cardType.name}</MenuItem>;
      console.log("PaymentMethod", card.userid, {
        cardD: card.cardType
          .filter(({ cardtype_id }) => cardtype_id === e.target.value)
          .map((card) => card),
        card_id: card.id,
      });

      setMyCard({
        ...myCard,
        cardid: card.id,
        cardname: card.cardType
          .filter(({ cardtype_id }) => cardtype_id === e.target.value)
          .map(({ name }) => name)[0],
      });

      //   return (
      // cardD: card.cardType
      //   .filter(({ cardtype_id }) => cardtype_id === e.target.value)
      //   .map((card) => card),
      // card_id: card.id,
      // <MenuItem value={card.id}>
      //   {console.log("menu:", card.id)}
      //   {card.cardType
      //     .filter(({ cardtype_id }) => cardtype_id === e.target.value)
      //     .map(({ name }) => name)}
      // </MenuItem>
      //   );
      //   setMyCard({
      //     cardD: card.cardType
      //       .filter(({ cardtype_id }) => cardtype_id === e.target.value)
      //       .map((card) => card),
      //     card_id: card.id,
      //   });
    });
  };
  // const handlecardCvc = (e) => {
  //     setcardCvc(e.target.value);
  //     // console.log(e.target.value);
  // }
  // const handlecardExpiry= (e) => {
  //     setcardExpiry(e.target.value);
  //     // console.log(e.target.value);
  // }
  // const handlecardBalance = (e) => {
  //     setcardBalance(e.target.value);
  //     // console.log(e.target.value);
  // }
  // const handleuserId = (e) => {
  //     setuserId(e.target.value);
  //     // console.log(e.target.value);
  // }

  const save = async (e) => {
    let data = {
      userid: user_id,
      amount: subtotal,
      cardid: card_id,
    };

    e.preventDefault();
    try {
      console.log(cartToCheckOut);
      const AuthStr = "Bearer " + localStorage.getItem("token");
      const formData = new FormData();
      formData.append("transaction", data);
      formData.append("cart", cartToCheckOut);
      const response = await axios
        .post(`${BASE}`, data, {
          headers: { Authorization: AuthStr },
        })

        .then(function (response) {
          console.log(response.data.message);
          SEND_REQUEST("POST", response.data.message, cartToCheckOut);
          setClose(false);
        });
    } catch (e) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Cannot save payment!",
      });
    }
  };

  return (
    <>
      <Modal show={isClose} animation={false}>
        <Form>
          <Modal.Header>
            <Modal.Title>
              <Box sx={{ my: 1 }}>
                <Typography color="textPrimary" variant="h4">
                  {`Checkout`}
                </Typography>
                <Typography color="textSecondary" gutterBottom variant="body2">
                  Payment:
                </Typography>
              </Box>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormControl variant="standard" sx={{ m: 1, minWidth: "100%" }}>
              <InputLabel id="demo-simple-select-standard-label">
                Payment Method
              </InputLabel>
              <Select
                labelId="demo-simple-select-standard-label"
                id="demo-simple-select-standard"
                label="cardtype"
                onChange={(e) => {
                  handleChange(e);
                  handlecardType(e);
                }}
              >
                <MenuItem value={1}>Credit</MenuItem>
                <MenuItem value={2}>Debit</MenuItem>
              </Select>
            </FormControl>

            <InputLabel id="demo-simple-select-standard-label">
              Select you Card
            </InputLabel>
            <Select
              labelId="demo-simple-select-standard-label"
              id="demo-simple-select-standard"
              sx={{ m: 1, minWidth: "100%" }}
              label="card"
              onChange={(e) => {
                //   handleChange(e);
                //   handlecardType();
                console.log("card id:", e.target.value);
                setCardId(e.target.value);
              }}
            >
              <MenuItem value={0} disabled>
                Select your card
              </MenuItem>
              {myCard && (
                <MenuItem value={myCard.cardid}>
                  {console.log("menu:", myCard.cardname)}
                  {myCard.cardname}
                </MenuItem>
              )}
            </Select>

            <TextField
              fullWidth
              label="Subtotal"
              margin="normal"
              name="Subtotal"
              variant="outlined"
              aria-readonly={true}
              value={subtotal}
            />

            {/* <TextField
                fullWidth
                label="Name"
                margin="normal"
                name="cardName"
                variant="outlined"
                required
                onChange={handlecardName}
              />
              <TextField
                fullWidth
                label="Number"
                margin="normal"
                name="cardNumber"
                variant="outlined"
                required
                onChange={handlecardNumber}
              />
              
              <TextField
                fullWidth
                label="Cvc"
                margin="normal"
                name="cardCvc"
                variant="outlined"
                required
                onChange={handlecardCvc}
              />

              <TextField
                fullWidth
                label="Expiry"
                margin="normal"
                name="cardExpiry"
                variant="outlined"
                required
                onChange={handlecardExpiry}
              />

              <TextField
                fullWidth
                label="Balance"
                margin="normal"
                name="cardBalance"
                variant="outlined"
                required
                onChange={handlecardBalance}
                disabled={isSelect}
              />

              <TextField
                fullWidth
                label="User ID"
                margin="normal"
                name="userId"
                variant="outlined"
                required
                onChange={handleuserId}
              /> */}
          </Modal.Body>
          <Modal.Footer>
            <Button
              color="primary"
              disabled={false}
              fullWidth
              size="large"
              type="submit"
              variant="contained"
              className="mb-2"
              onClick={save}
            >
              Pay
            </Button>

            <Button
              color="error"
              disabled={false}
              fullWidth
              size="large"
              type="submit"
              variant="contained"
              className="mb-2"
              onClick={handleClose}
            >
              Cancel
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}

export default PaymentMethod;
