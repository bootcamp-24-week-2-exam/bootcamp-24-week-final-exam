import React, { useState } from "react";
import { Box, Button, TextField, Typography } from "@mui/material";
import { Modal, Form, Container, Row, Col } from "react-bootstrap";

const ProductEdit = ({ SEND_REQUEST, product, dispatch, state }) => {
  const initialImg = "";

  const [images, setImages] = useState(
    // state.product.image === null ? initialImg : state.product.image.split(",")
    state.product.image === null ? initialImg : state.product.image
  );
  const [title, setTitle] = useState("");
  const [isSelect, setSelect] = useState(false);

  const handleChange = (e) => {
    // dispatch({
    //   type: "CHANGE_INPUT",
    //   payload: { name: e.target.name, value: e.target.value },
    // });

    state.product = {
      ...state.product,
      [e.target.name]: e.target.value,
    };
  };

  const hadleImage = (img) => {
    if (isSelect) {
      //   setSelect(false);
      //   if (images.length > img) {
      //   return "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png";
      //   //   } else {
      return URL.createObjectURL(images);
      //   }
    } else {
      return `http://localhost:8080/img/${images}`;
    }
  };

  const handleSaveSubmit = (e) => {
    e.preventDefault();
    const toSend = { data: state.product, image: images };

    SEND_REQUEST("PUT", toSend, product.currentPage, product.productPerPage);

    setImages(initialImg);

    dispatch({
      type: state.action,
      payload: {
        do: state.action,
        isOpenClose: false,
        productData: state.product,
      },
    });
  };
  const handleClose = () => {
    // e.preventDefault();
    setImages(initialImg);
    if (state.action === "edit") {
      dispatch({
        type: state.action,
        payload: {
          do: state.action,
          isOpenClose: false,
          productData: state.product,
        },
      });
    } else if (state.action === "add") {
      dispatch({
        type: state.action,
        payload: {
          do: state.action,
          isOpenClose: false,
        },
      });
    } else if (state.action === "view") {
      dispatch({
        type: state.action,
        payload: {
          do: state.action,
          isOpenClose: false,
          productData: state.product,
        },
      });
    }
  };

  return (
    <>
      <Modal show={state.isOpen} onHide={handleClose} animation={false}>
        <Form onSubmit={handleSaveSubmit}>
          <Modal.Header closeButton>
            <Modal.Title>
              <Box sx={{ my: 1 }}>
                <Typography color="textPrimary" variant="h4">
                  {`${title}Product`}
                </Typography>
                <Typography color="textSecondary" gutterBottom variant="body2">
                  `Enter your product details here`
                </Typography>
              </Box>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Box sx={{}}>
              <input
                type="file"
                name="file"
                // multiple
                accept="image/jpeg"
                onChange={(e) => {
                  setSelect(true);
                  //   setImages([
                  //     ...images,
                  //     URL.createObjectURL(...e.target.files),
                  //   ]);
                  setImages(e.target.files[0]);
                }}
                className="mb-1"
                hidden={state.action === "edit" ? false : true}
              />
              {images !== "" && (
                <Container>
                  <Row>
                    <Col>
                      <img
                        width="100"
                        height="100"
                        alt="image1"
                        src={hadleImage(0)}
                      />
                    </Col>
                    {/* <Col>
                      <img
                        width="100"
                        height="100"
                        alt="image2"
                        src={hadleImage(1)}
                      />
                    </Col>
                    <Col>
                      <img
                        width="100"
                        height="100"
                        alt="image3"
                        src={hadleImage(2)}
                      />
                    </Col> */}
                    <Button
                      className="mt-2"
                      color="error"
                      hidden={state.action === "edit" ? false : true}
                      onClick={() => {
                        setImages(initialImg);
                      }}
                    >
                      Remove This Image
                    </Button>
                  </Row>
                </Container>
              )}
              <Container hidden={state.action === "edit" ? false : true}>
                <Row>Select your product image/jpeg</Row>
              </Container>
            </Box>

            <Box sx={{}}>
              <TextField
                fullWidth
                label="Product Name"
                margin="normal"
                name="productName"
                onChange={handleChange}
                defaultValue={state.product.productName}
                variant="outlined"
                InputProps={{
                  readOnly: state.action === "view" ? true : false,
                }}
                required
              />
              <TextField
                fullWidth
                // multiline
                // maxRows={4}
                label="Description"
                margin="normal"
                name="description"
                onChange={handleChange}
                defaultValue={state.product.description}
                variant="outlined"
                InputProps={{
                  readOnly: state.action === "view" ? true : false,
                }}
                required
              />
              <TextField
                fullWidth
                label="Quantity"
                margin="normal"
                name="quantity"
                onChange={handleChange}
                defaultValue={state.product.quantity}
                type="number"
                variant="outlined"
                InputProps={{
                  readOnly: state.action === "view" ? true : false,
                }}
                required
              />
              <TextField
                fullWidth
                label="Price"
                margin="normal"
                name="price"
                onChange={handleChange}
                defaultValue={state.product.price}
                variant="outlined"
                InputProps={{
                  readOnly: state.action === "view" ? true : false,
                }}
                required
              />
              <TextField
                fullWidth
                label="Category"
                margin="normal"
                name="category"
                onChange={handleChange}
                defaultValue={state.product.category}
                variant="outlined"
                InputProps={{
                  readOnly: state.action === "view" ? true : false,
                }}
                required
              />
              <TextField
                fullWidth
                label="Brand"
                margin="normal"
                name="brand"
                onChange={handleChange}
                defaultValue={state.product.brand}
                variant="outlined"
                InputProps={{
                  readOnly: state.action === "view" ? true : false,
                }}
                required
              />
              <TextField
                fullWidth
                label="Size"
                margin="normal"
                name="size"
                onChange={handleChange}
                defaultValue={state.product.size}
                variant="outlined"
                InputProps={{
                  readOnly: state.action === "view" ? true : false,
                }}
                required
              />
              <TextField
                fullWidth
                label="Color"
                margin="normal"
                name="color"
                onChange={handleChange}
                defaultValue={state.product.color}
                variant="outlined"
                InputProps={{
                  readOnly: state.action === "view" ? true : false,
                }}
                required
              />
            </Box>
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                ml: -1,
              }}
            >
              {/* <Checkbox
                checked={state.product.policy}
                name="policy"
                onChange={handleChange}
              /> */}
              {/* <Typography color="textSecondary" variant="body2">
                I have read the
                 <NextLink href="#" passHref>
                <Link color="primary" underline="always" variant="subtitle2">
                  Terms and Conditions
                </Link>
                </NextLink>
              </Typography> */}
            </Box>
          </Modal.Body>
          <Modal.Footer>
            <Box sx={{ py: 2, width: "100%" }}>
              <Button
                color="primary"
                disabled={false}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                className="mb-2"
                // onClick={handleSaveSubmit}
                hidden={state.action === "edit" ? false : true}
              >
                Save
              </Button>
              <Button
                color="error"
                disabled={false}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                className="mb-2"
                onClick={handleClose}
              >
                Cancel
              </Button>
            </Box>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};
export default ProductEdit;
