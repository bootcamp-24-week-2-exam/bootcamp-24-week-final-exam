import * as React from "react";
import { useState } from "react";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { Modal, Form } from "react-bootstrap";
import { Box, Button, TextField, Typography } from "@mui/material";
import axios from "../api/axios";
import Swal from "sweetalert2";
import Moment from "moment";

export default function Report() {
  const [value, setValue] = React.useState(null);
  const [value2, setValue2] = React.useState(null);
  const [isClose, setClose] = useState(true);

  console.log(value);

  const handleClose = (e) => {
    e.preventDefault();
    setClose(false);
  };

  const handleGeneratePDF = async (e) => {
    e.preventDefault();
    const AuthStr = "Bearer " + localStorage.getItem("token");
    const response = await axios
      .get(`/transaction/date/pdf?start=${value}&end=${value2}`)
      .then((res) => {
        console.log("pdf:", res.data);
        const filename = res.headers
          .get("Content-Disposition")
          .split("filename=")[1];
        res.blob().then((blob) => {
          let url = window.URL.createObjectURL(blob);
          let a = document.createElement("a");
          a.href = url;
          a.download = filename;
          a.click();
        });
      })
      .catch((err) => {
        console.log(err);
        Swal.fire("ERROR", "Something wrong...", "error");
      });
    setClose(false);
  };

  return (
    <>
      <Modal show={isClose} animation={false}>
        <Form>
          <Modal.Header>
            <Modal.Title>
              <Box sx={{ my: 1 }}>
                <Typography color="textPrimary" variant="h4">
                  Report
                </Typography>
                <Typography color="textSecondary" gutterBottom variant="body2">
                  reports:
                </Typography>
              </Box>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker
                label="Start Date"
                value={value}
                onChange={(newValue) => {
                  setValue(newValue);
                }}
                renderInput={(params) => <TextField {...params} />}
              />
              <DatePicker
                label="End Date"
                value={value2}
                onChange={(newValue) => {
                  setValue2(newValue);
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
          </Modal.Body>
          <Modal.Footer>
            <Button
              color="primary"
              disabled={false}
              fullWidth
              size="large"
              type="submit"
              variant="contained"
              className="mb-2"
              //   href="http://localhost:8080/transaction/date/pdf?start=" && value && "&end=" && value2
              href={`http://localhost:8080/transaction/date/pdf?start=${Moment(
                value
              ).format("DD-MM-YYYY")}&end=${Moment(value2).format(
                "DD-MM-YYYY"
              )}`}
              //   onClick={handleGeneratePDF}
            >
              Generate Report - PDF
            </Button>

            <Button
              color="error"
              disabled={false}
              fullWidth
              size="large"
              type="submit"
              variant="contained"
              className="mb-2"
              onClick={handleClose}
            >
              Cancel
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
