import React, { useState } from "react";
import { Modal, Button } from "react-bootstrap";
import axios from "axios";
import Swal from "sweetalert2";
import "../styles/ProductCsv.css";

const ProductCsv = ({ product, SEND_REQUEST, search, adminId }) => {
  // useEffect(() => {});
  const CSV_URL = `http://localhost:8080/product/csv/upload?id=${adminId}`;

  const [csv, setCSVModal] = useState(false);
  const handleCloseCSV = () => setCSVModal(false);
  const handleShowCSV = () => setCSVModal(true);

  const [csvFile, SetCsvFile] = useState();

  const uploadCSV = async (e) => {
    e.preventDefault();
    try {
      let formData = new FormData();
      formData.append("file", csvFile[0]);
      const response = await axios
        .post(CSV_URL, formData)
        .then(function (response) {
          handleCloseCSV();
          Swal.fire(
            "Csv File Uploaded",
            "Successfully Added the products!",
            "success"
          );
          SEND_REQUEST(
            "GET",
            search,
            product.currentPage,
            product.productPerPage
          );
        })
        .catch(function (response) {
          handleCloseCSV();
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Could not upload the file",
          });
        });
    } catch (e) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Please choose a CSV file!",
      });
    }

    SetCsvFile();
  };
  return (
    <>
      <Button
        className="m-2 float-end"
        variant="primary"
        type="submit"
        size="lg"
        onClick={handleShowCSV}
      >
        Upload CSV
      </Button>

      <Modal show={csv} onHide={handleCloseCSV} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Upload Product Using CSV</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input
            type="file"
            name="file"
            accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
            onChange={(e) => SetCsvFile(e.target.files)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button type="button" variant="danger" onClick={handleCloseCSV}>
            Cancel
          </Button>
          <Button
            variant="primary"
            onClick={(e) => {
              uploadCSV(e);
            }}
          >
            Upload
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default ProductCsv;
