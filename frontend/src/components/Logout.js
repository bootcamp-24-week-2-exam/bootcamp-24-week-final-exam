import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const Logout = () => {

    const navigate = useNavigate();
    // clear localstorage.
    localStorage.clear();
    // navigate back to login page.
    useEffect(() => {
        navigate("/login");
    }, [navigate]);
    

    return(
        <div className="Logout">
        </div>
    );
}

export default Logout;