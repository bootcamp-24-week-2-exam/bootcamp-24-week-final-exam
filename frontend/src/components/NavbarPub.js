// import { useEffect } from "react";
import {
  Container,
  Nav,
  Navbar,
  NavDropdown,
  // NavDropdown,
  Offcanvas,
} from "react-bootstrap";
import "../styles/NavbarPub-style.css";

// login and register bar.
const LoginRegister = ({ expandto }) => {
  if (
    (localStorage.getItem("token") !== null || localStorage.getItem("token")) &&
    (localStorage.getItem("status") !== false ||
      localStorage.getItem("status") !== "") &&
    localStorage.getItem("role") === "ROLE_ADMIN"
  ) {
    return (
      <Nav className="justify-content-center">
        <NavDropdown
          title="Menu"
          id={`offcanvasNavbarDropdown-expand-${expandto}`}
        >
          <NavDropdown.Item href="/admin/profile">Profile</NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
        </NavDropdown>
      </Nav>
    );
  } else if (
    (localStorage.getItem("token") !== null || localStorage.getItem("token")) &&
    (localStorage.getItem("status") !== false ||
      localStorage.getItem("status") !== "") &&
    localStorage.getItem("role") === "ROLE_CLIENT"
  ) {
    return (
      <Nav className="justify-content-center">
        <NavDropdown
          title="Menu"
          id={`offcanvasNavbarDropdown-expand-${expandto}`}
        >
          <NavDropdown.Item href="/client/profile">Profile</NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
        </NavDropdown>
      </Nav>
    );
  } else {
    return (
      <Nav className="justify-content-end pe-3">
        <Nav.Link href="/login">Login</Nav.Link>
        <Nav.Link href="/register">Register</Nav.Link>
      </Nav>
    );
  }
};
const NavbarPub = ({ imageLoc }) => {
  return (
    <>
      {["xl"].map((expand) => (
        <Navbar key={expand} bg="light" expand={expand} className="mb-3">
          <Container fluid>
            <Navbar.Brand href="#">One Stop Shop</Navbar.Brand>
            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-${expand}`}
              aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
              placement="end"
            >
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                  Offcanvas
                </Offcanvas.Title>
              </Offcanvas.Header>
              <Offcanvas.Body>
                <Nav className="justify-content-end flex-grow-1 pe-3">
                  <Nav.Link href="/">Home</Nav.Link>
                  <Nav.Link href="/about">About</Nav.Link>
                  <Nav.Link href="/contact">Contacts</Nav.Link>
                  <Nav.Link href="/client/cart">
                    <img className="cart-style" src={imageLoc} alt="Cart" />
                  </Nav.Link>
                  <Nav.Link href="/card">Card</Nav.Link>
                </Nav>
                <LoginRegister expandto={expand} />
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
      ))}
    </>
  );
};

export default NavbarPub;
