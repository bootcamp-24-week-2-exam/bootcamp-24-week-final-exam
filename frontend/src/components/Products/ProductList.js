import React, { useContext, useEffect } from "react";
// import axios from '../../api/axios';
import ProductContext from "../../context/ProductContext";
import ReactPaginate from "react-paginate";
import { Card } from "react-bootstrap";
import "../../styles/ProductListStyle.css";
import { useNavigate } from "react-router-dom";

const ProductDisplay = () => {
  const IMG_PATH = "http://localhost:8080/img/";
  // implment productContext.
  const { productDetails } = useContext(ProductContext);
  const navigate = useNavigate();

  const handleCard = (prod) => {
    // set selected product
    localStorage.setItem("product", JSON.stringify(prod));
    // console.log(prod);
    navigate("/product/overview");
  };

  // check if product is set.
  // useEffect(() => {
  //   console.log(product);
  // }, [product]);

  useEffect(() => {
    console.log("sam:", productDetails);
  }, []);

  return productDetails.map((prod) => {
    if (prod !== null) {
      // console.log(prod);
      return (
        <div className="col-sm-12 col-md-2 v my-2" key={prod.productID}>
          <Card
            className="card-style card-size"
            onClick={() => {
              handleCard(prod);
            }}
          >
            <Card.Img
              variant="top"
              src={
                prod.image != null
                  ? `${IMG_PATH}${prod.image}`
                  : "../images/shopping.jpg"
              }
            />
            <Card.Body>
              <Card.Title className="prodname-style">
                {prod.productName}
              </Card.Title>
              <Card.Text className="price-style">
                ₱ {prod.price}
                {/* set Product details to selected card.*/}
              </Card.Text>
              {/* <Button variant="primary">Add to Cart</Button> */}
            </Card.Body>
          </Card>
        </div>
      );
    } else {
      return (
        <div className="col-sm-12 col-md-2 v my-2" key={prod.productID}>
          <h3>No products found.</h3>
        </div>
      );
    }
  });
};

const ProductList = () => {
  // implment productContext.
  const {
    productDetails,
    setProductDetails,
    productPage,
    setProductPage,
    size,
    totalpages,
    endpoint,
    setEndpoint,
  } = useContext(ProductContext);
  // store pages in an array.
  let pages = [];

  for (let i = 1; i <= totalpages; i++) {
    pages.push(i);
  }

  // console.log(pages);

  const handlePage = (page) => {
    // axios
    //   .get(`/product/?page=${page.selected}&size=${size}`)
    //   .then((response) => {
    //     console.log(response.data.content);
    //     // set productDetails to response.data.content.
    //     setProductDetails([response.data.content]);
    //     // set productPage to page.
    //     setProductPage(page.selected);
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
    setProductPage(page.selected);
    setEndpoint(`/product/?page=${page.selected}&size=${size}`);
  };

  useEffect(() => {
    // console.log(productDetails);
    console.log("Product Page: " + parseInt(productPage + 1));
    console.log(productDetails);
  }, [
    productPage,
    setProductPage,
    productDetails,
    setProductDetails,
    endpoint,
    setEndpoint,
  ]);

  return (
    <div className="container">
      <div className="row m-1 ">
        <ProductDisplay />
      </div>

      <ReactPaginate
        previousLabel={"<<Prev"}
        nextLabel={"Next>>"}
        breakLabel={"..."}
        pageCount={totalpages}
        marginPagesDisplayed={3}
        pageRangeDisplayed={2}
        onPageChange={handlePage}
        containerClassName={"pagination justify-content-center"}
        pageClassName={"page-item"}
        pageLinkClassName={"page-link"}
        previousClassName={"page-item"}
        previousLinkClassName={"page-link"}
        nextClassName={"page-item"}
        nextLinkClassName={"page-link"}
        breakClassName={"page-item"}
        breakLinkClassName={"page-link"}
        activeClassName={"active"}
      />

      {/* {
        pages.map((page, index) => {
          return(
            <button key={index} onClick={() => {handlePage(page-1)}}>{page}</button>
          );
        })
      } */}
    </div>
  );
};

export default ProductList;
