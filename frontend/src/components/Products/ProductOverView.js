import axios from "../../api/axios";
import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import NavbarPub from "../NavbarPub";
// import "../../styles/ProductOverView-style.css";

const ProductDisplay = () => {
  const IMG_PATH = "http://localhost:8080/img/";
  // product details.
  let prod = JSON.parse(localStorage.getItem("product"));
  const [prodQuantity, setProdQuantity] = useState(1);

  const navigate = useNavigate();

  const handleAddQuantity = () => {
    if (prodQuantity < prod.quantity) {
      let sum = prodQuantity + 1;
      setProdQuantity(sum);
      console.log(prodQuantity);
    }
  };

<<<<<<< HEAD
		// post request.
		// authenticate and set Jwt response and set Login to true..
		const AuthStr = 'Bearer ' + localStorage.getItem("token"); 
        axios.post("/cart", addToCart, {
			headers: {Authorization: AuthStr}
		})
        .then((response) => {
            console.log(response)
        })
        .catch((error) => {
			console.log(AuthStr);
            console.log(error);
        });
		navigate("/client/cart");
	}
=======
  const handleMinusQuantity = () => {
    if (prodQuantity > 1) {
      let sum = prodQuantity - 1;
      setProdQuantity(sum);
      console.log(prodQuantity);
    }
  };
>>>>>>> 9aba0a2aa35a0c9d3e2202a2146dab44e85ecb68

  useEffect(() => {
    localStorage.setItem("quantity", prodQuantity);
    // console.log(localStorage.getItem("quantity"));
  }, [prodQuantity]);

  const handleAddtoCart = () => {
    let addToCart = {
      user_id: parseInt(localStorage.getItem("userid")),
      product_id: parseInt(prod.productID),
      quantity: parseInt(prodQuantity),
      status: 1,
    };

    // post request.
    // authenticate and set Jwt response and set Login to true..
    const AuthStr = "Bearer " + localStorage.getItem("token");
    axios
      .post("/cart", addToCart, {
        headers: { Authorization: AuthStr },
      })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
    navigate("/client/cart");
  };

  return (
    <div className="container">
      <NavbarPub imageLoc={"../images/cart-icon.png"} />
      <div className="card p-5 bg">
        <div className="container-fliud">
          <div className="wrapper row">
            <div className="preview col-md-6">
              <div className="preview-pic tab-content">
                <div className="tab-pane active" id="pic-1">
                  <img
                    src={
                      prod.image != null
                        ? `${IMG_PATH}${prod.image}`
                        : "../images/shopping.jpg"
                    }
                    alt="item"
                  />
                </div>
                {/* <div className="tab-pane" id="pic-2"><img src="http://placekitten.com/400/252" /></div>
							<div className="tab-pane" id="pic-3"><img src="http://placekitten.com/400/252" /></div>
							<div className="tab-pane" id="pic-4"><img src="http://placekitten.com/400/252" /></div>
							<div className="tab-pane" id="pic-5"><img src="http://placekitten.com/400/252" /></div> */}
              </div>
              {/* <ul className="preview-thumbnail nav nav-tabs">
							<li className="active"><a data-target="#pic-1" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
							<li><a data-target="#pic-2" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
							<li><a data-target="#pic-3" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
							<li><a data-target="#pic-4" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
							<li><a data-target="#pic-5" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
						  </ul> */}
            </div>
            <div className="details col-md-6">
              <h3 className="product-title">{prod.productName}</h3>
              <h4 className="price">
                current price:{" "}
                <span className="text-primary">₱ {prod.price}</span>
              </h4>
              <h5 className="sizes">Size: {prod.size}</h5>
              <h5 className="sizes">Category: {prod.category}</h5>
              <h5 className="sizes">Color: {prod.color}</h5>
              <h5 className="sizes">Quantity: {prod.quantity}</h5>
              <Button
                className="w-25 mt-2 btn-secondary"
                onClick={handleAddQuantity}
              >
                +
              </Button>
              <p className="border w-25 mt-2">{prodQuantity}</p>
              <Button
                className="w-25 btn-secondary"
                onClick={handleMinusQuantity}
              >
                -
              </Button>
              {/* <input type="number" id="quantity" placeholder="input quantity" name="quantity" min="1" max={prod.quantity} value={prodQuantity} onChange={handleProdQuantity} style={{width: '200px', color: ''}}/> */}
              <p className="sizes mt-3">Description:</p>
              <p className="product-description mt-0">{prod.description}</p>
              <Button
                className="add-to-cart btn btn-default mb-5 p-3 w-50 float-left"
                type="button"
                onClick={handleAddtoCart}
              >
                add to cart
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const ProductOverView = () => {
  return <ProductDisplay />;
};

export default ProductOverView;
