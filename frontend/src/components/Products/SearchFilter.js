import React, { useContext, useEffect } from 'react'
import { Button, Form } from 'react-bootstrap';
import ProductContext from '../../context/ProductContext';
import "../../styles/SearchFilter-style.css";

const SearchFilter = () => {

  // const [filterby, setFilterBy] = useState(1);
  // const [sortby, setSortBy] = useState(1);
  const { filterby, setFilterBy, sortby, setSortBy, search, setSearch, endpoint, setEndpoint, productPage, size } = useContext(ProductContext);
  
  

  const handleSearch = (e) => {
    setSearch(e.target.value);
    // console.log(search);
  }

  const handleFilterBy = (e) => {
    setFilterBy(e.target.value);
    // console.log(filterby);
  }

  const handleSortBy = (e) => {
    setSortBy(e.target.value);
    // console.log(sortby);
    // set to current endpoint.
    setEndpoint(e.target.value);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(search);
    console.log(filterby);
    let modifiedEndpoint = `/product/sort/${search}?page=0&size=10&sort=${filterby},asc`;
    setEndpoint(modifiedEndpoint);
    // // use axios to search.
    // axios.get({})
  }

  useEffect(() => {
    console.log(endpoint);
  }, [endpoint, setEndpoint, sortby, setSortBy, filterby, setFilterBy]);

  return (
    <div className='SearchFilter'>
        <br/>
        <Form className="d-flex search-field" onSubmit={handleSubmit}>
          <Form.Select className='filter-field' aria-label="Default select example" value={filterby} onChange={handleFilterBy}>
            <option>Filter by</option>
            <option value="productName">Product name</option>
            <option value="category">Category(top/bottom)</option>
            <option value="brand">Product Brand</option>
        </Form.Select>
        <Form.Select className='sort-field' aria-label="Default select example" value={sortby} onChange={handleSortBy}>
            <option value={`/product/?page=${productPage}&size=${size}`}>Sort by</option>
            <option value="/product/sort/?page=0&size=10&sort=productName,asc">Ascending</option>
            <option value="/product/sort/?page=0&size=10&sort=productName,desc">Descending</option>
        </Form.Select>
            <Form.Control
                type="search"
                placeholder="Search"
                className="me-2"
                aria-label="Search"
                value={search}
                onChange={handleSearch}
                required
                />
            <Button variant="outline-success" type='submit'>Search</Button>
        </Form>
    </div>
  );
};

export default SearchFilter