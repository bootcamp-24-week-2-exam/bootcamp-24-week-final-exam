export const INITIAL_STATE = {
  isLoggedIn: false,
  isLoading: false,
  isError: false,
  isOpen: false,
  action: "",
  product: {
    image: "",
    productName: "",
    description: "",
    quantity: "",
    price: "",
    sku: "",
    category: "",
    brand: "",
    size: "",
    color: "",
    createdAt: "",
  },
};

export const productReducer = (state, action) => {
  switch (action.type) {
    case "CHANGE_INPUT":
      return { ...state.product, [action.payload.name]: action.payload.value };
    case "add":
      return {
        ...state,
        action: action.payload.do,
        isOpen: action.payload.isOpenClose,
      };
    case "edit":
      return {
        ...state,
        action: action.payload.do,
        isOpen: action.payload.isOpenClose,
        product: action.payload.productData,
      };
    case "view":
      return {
        ...state,
        action: action.payload.do,
        isOpen: action.payload.isOpenClose,
        product: action.payload.productData,
      };
    case "CLOSE":
      return {};
    default:
      return state;
  }
};

export default productReducer;
