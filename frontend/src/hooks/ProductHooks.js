export const INITIAL_STATE = {
  isLoggedIn: false,
  isLoading: false,
  isError: false,
  isOpen: false,
  action: "",
};

export const ProductHooks = (prevState, action) => {
  console.log("hooks: ", action.type);
  switch (action.type) {
    case "IS_LOADING":
      return {
        ...prevState,
        isLoading: true,
      };
    case "IS_NOT_LOADING":
      return {
        ...prevState,
        isLoading: false,
      };
    case "SUCCESS":
      return {
        ...prevState,
        isError: false,
        isLoading: false,
      };
    case "ERROR":
      return {
        ...prevState,
        isError: true,
        isLoading: false,
      };
    case "OPEN":
      return {
        isOpen: true,
        action: action.do,
      };
    case "CLOSE":
      return {
        isOpen: false,
        action: action.do,
      };
    default:
      throw new Error();
  }
};
